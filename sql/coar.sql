-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 17-07-2020 a las 15:52:35
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `coar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos`
--

DROP TABLE IF EXISTS `archivos`;
CREATE TABLE IF NOT EXISTS `archivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `titulo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `ruta` varchar(1024) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `size` int(50) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

DROP TABLE IF EXISTS `articulos`;
CREATE TABLE IF NOT EXISTS `articulos` (
  `IdArticulo` int(11) NOT NULL AUTO_INCREMENT,
  `Tipo_Art` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `Tags_Art` varchar(1024) COLLATE utf8_spanish_ci NOT NULL,
  `id_usuario` int(10) NOT NULL,
  `FechaCreacion_Art` datetime NOT NULL,
  `Visitas_Art` int(10) NOT NULL DEFAULT 0,
  `FechaPublicacion_Art` datetime(6) DEFAULT NULL,
  `Titulo_Art` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `Introtext_Art` longtext COLLATE utf8_spanish_ci NOT NULL,
  `Fulltext_Art` longtext COLLATE utf8_spanish_ci NOT NULL,
  `UrlImg_Art` varchar(1024) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FechaModificacion_Art` datetime DEFAULT NULL,
  `Idusuariomodifica_Art` int(11) DEFAULT NULL,
  `Activo_Art` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdArticulo`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`IdArticulo`, `Tipo_Art`, `Tags_Art`, `id_usuario`, `FechaCreacion_Art`, `Visitas_Art`, `FechaPublicacion_Art`, `Titulo_Art`, `Introtext_Art`, `Fulltext_Art`, `UrlImg_Art`, `FechaModificacion_Art`, `Idusuariomodifica_Art`, `Activo_Art`) VALUES
(2, 'P', 'lorem ipsum,test,php,1st article', 1, '2020-07-15 08:54:00', 0, NULL, 'Titulo de ejemplo', '<p class=\"ql-align-justify\"><strong>Cuerpo del artículo:</strong> L<em>orem ipsum dolo</em>r sit am<strong>et, consectetur adipi</strong>scing elit. Etiam dictum suscipit sagittis. Integer egestas ac urna ac rutrum. Donec consequat aliquam sapien nec placerat. Curabitur nec sagittis nisl. Nam vestibulum quam sed lobortis mattis. Praesent eget purus tincidunt, lobortis libero sit amet, imperdiet leo. <em>Cras lacinia, nis</em>i eget lacinia maximus, odio nisi consequat libero, quis laoreet urna ante et quam. Donec condimentum lacus arcu, laoreet luctus arcu tempor a. In facilisis facilisis nunc et tempor.</p><p class=\"ql-align-justify\">Phasellus accumsan metus ut lorem malesuada tincidunt. Cras lobortis porttitor metus. Cras volutpat est ac nibh ultrices rhoncus. Vestibulum ut aliquet eros. Integer aliquet tellus ipsum, id volutpat erat scelerisque eget. Cras fermentum viverra ante, sed semper tellus facilisis varius. Sed id elit pellentesque, dignissim orci quis, fermentum enim. Curabitur aliquam viverra nisl, pulvinar rhoncus arcu auctor quis. Nam eleifend eleifend scelerisque. Donec tristique dui nec turpis tristique ultrices. Maecenas lobortis sem eget molestie egestas.</p><p class=\"ql-align-justify\">Integer vel diam interdum, faucibus turpis vel, iaculis magna. Donec rutrum, sapien eget egestas tincidunt, massa enim ornare orci, a elementum sapien nibh eget velit. Cras eu consectetur erat. Duis accumsan finibus ex. Aenean bibendum ante eget nulla vestibulum, id faucibus sem sollicitudin. Proin eget augue elit. Integer tristique, massa lacinia condimentum mollis, urna ante cursus risus, quis laoreet neque sem sit amet sapien. In ultrices, nulla eu imperdiet venenatis, mauris ante elementum mauris, id euismod ante urna ac nulla. In luctus felis at velit auctor efficitur. Integer quis urna lacus. Vestibulum pellentesque eget diam mollis maximus.</p><p class=\"ql-align-justify\"><strong>Mauris viverr</strong>a justo sit amet nunc finibus, in bibendum nibh mollis. Suspendisse sed libero leo. Morbi luctus semper pulvinar. Pellentesque suscipit finibus nibh, et eleifend odio iaculis nec. Curabitur nec tristique mi. Maecenas molestie diam quam, id rutrum nulla cursus nec. Praesent et commodo libero, et elementum turpis.</p><p class=\"ql-align-justify\">Phasellus suscipit dui et lorem euismod, molestie semper tortor tincidunt. Vestibulum odio ipsum, tempor sit amet lectus sit amet, pellentesque congue leo. Maecenas viverra congue nisl imperdiet dictum. Curabitur sed neque tincidunt, vehicula lorem quis, rutrum massa. Sed erat tellus, tempus sed laoreet quis, commodo vitae nibh. Ut fringilla eget purus ultricies ultricies. Quisque felis risus, porttitor ac tincidunt non, dignissim at dolor. Aliquam tincidunt finibus neque, sit amet malesuada lacus maximus sit amet. Sed ut efficitur orci, vitae egestas dolor. Vivamus non augue a diam pretium congue. Curabitur eu arcu lectus. Phasellus ac orci leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque volutpat pellentesque purus commodo eleifend.</p><p><br></p>', '<p>Texto Introductorio <em style=\"color: rgb(0, 0, 0);\">Lorem ipsum dolor sit amet, con</em><span style=\"color: rgb(0, 0, 0);\">sectetur adipiscing elit. Etiam dictum suscipit sagittis. Integer egestas ac urna ac rutrum. Donec consequat aliquam sapien nec placerat. Curabitur nec sagittis nisl. </span><strong style=\"color: rgb(0, 0, 0);\">Nam vestibulum quam sed</strong><span style=\"color: rgb(0, 0, 0);\"> lobortis mattis. Praesent eget purus tincidunt, lobortis libero sit amet, imperdiet leo. Cras lacinia, nisi eget lacinia maximus, odio nisi consequat libero, quis laoreet urna ante et quam. Donec condimentum lacus arcu, laoreet luctus arcu tempor a. In facilisis facilisis nunc et tempor.</span></p>', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calendario`
--

DROP TABLE IF EXISTS `calendario`;
CREATE TABLE IF NOT EXISTS `calendario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `className` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `allDay` tinyint(1) NOT NULL DEFAULT 0,
  `Iduser` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `calendario`
--

INSERT INTO `calendario` (`id`, `title`, `description`, `className`, `start`, `end`, `allDay`, `Iduser`) VALUES
(2, 'Acabar Pagina web', 'Culminar: Vista cliente de Archivos publicados y Artículos', 'bg-danger', '2020-07-13', '2020-07-17', 1, 1),
(1, 'Eventos de FullCalendar.js', 'Se puede reajustar eventos (longitud y dia), asignar colores, actualizar, crear y eliminar.', 'bg-default', '2020-06-30', '2020-07-05', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificaciones`
--

DROP TABLE IF EXISTS `notificaciones`;
CREATE TABLE IF NOT EXISTS `notificaciones` (
  `idNotificacion` int(11) NOT NULL,
  `mensaje` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `estado` bit(1) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `password` tinytext COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechaCaptura` date DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Los usuarios Anulados estan en 0',
  `isadministrador` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'podra crear usuarios',
  `direccion` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `ciudad` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `about` tinytext COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_usuario`) USING BTREE,
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `username`, `nombre`, `apellido`, `email`, `password`, `fechaCaptura`, `activo`, `isadministrador`, `direccion`, `ciudad`, `telefono`, `about`) VALUES
(1, 'maribel', 'Maribel', 'Rojas', 'rojasperezmaribel@gmail.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', '2019-10-13', 1, 1, 'Jose Luis Bustamante y Rivero,', 'Arequipa, Peru', '954182364', 'Líder del ministerio DOULOS. :D\r\n\r\n'),
(2, 'Owen H.', 'Owen', 'Roque', 'owenhrs@gmail.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', '2020-05-18', 1, 1, 'Av. Arequipa 701 - A', 'Arequipa, Peru', '924387067', '<3');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
