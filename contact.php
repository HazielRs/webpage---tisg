<?php
session_start();
require_once (dirname(__FILE__) . '/clases/conexion.php');
$c = new conectar();
?>
<!doctype html>
<html lang="es_PE">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="img/favicon.png" type="image/png">
    <title>Ministerio Doulos - Id y hacer Discipulos a todas las naciones</title>
    <base href="<?php echo $c->urlbase;?>">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">

    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/my.css">
    <link rel="stylesheet" href="css/redessociales.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/835ac9bcc2f5ac466178e6578/c48fd358b3ec665fb141d17c7.js");</script>
</head>
<body>
<?php include ("snippets/navbar.php"); ?>

<section id="contact">
    <div id="webcontent" class="webcontent">
<!--================ CONTENIDO A PARTIR DE AQUI =================-->
        <!--================Contact Area =================-->
        <div class="row" style="margin-top: 10em;"></div>
        <section class="contact_area p_20 ">
            <div class="container">
                <div id="mapBox" class="mapBox"
                     data-lat="-16.4248622"
                     data-lon="-71.5259675"
                     data-zoom="17"
                     data-info="Av. Paseo de La Cultura 129, José Luis Bustamante y Rivero, Arequipa, Perú."
                     data-marker="Ministerio Doulos Arequipa - Perú"
                     data-mlat="-16.4248674"
                     data-mlon="-71.5237788">
                </div>

                <div class="row">
                    <div class="span11">
                        <div id="map"></div>
                    </div>
                </div>
                <div class="row title_item_h4">
                    <h2>Contáctenos</h2>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="contact_info">
                            <div class="info_item">
                                <i class="lnr lnr-home"></i>
                                <h6>Arequipa, Perú</h6>
                                <p>Av. Paseo de La Cultura 129, José Luis Bustamante y Rivero</p>
                            </div>
                            <div class="info_item">
                                <i class="lnr lnr-phone-handset"></i>
                                <h6><a href="#">(+51)989281876, (+51)976227782</a></h6>
                                <p>Martes a Sábado de 10:00am to 07:00pm</p>
                            </div>
                            <div class="info_item">
                                <i class="lnr lnr-envelope"></i>
                                <h6><a href="#">mision@id.org.pe</a></h6>
                                <p>Envie su consulta en cualquier tiempo!</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <form class="row contact_form" action="contact_process.php"
                              method="post" id="contactForm" novalidate="novalidate">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Ingrese su nombre">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Ingrese su E-mail">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Ingrese un asunto">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="message" id="message" rows="1" placeholder="Ingrese un mensaje"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <button type="submit" value="submit" class="btn submit_btn">Enviar Mensaje</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!--================Contact Area =================-->

        <!--================Instagram Area =================-->
        <?php require_once "snippets/InstaArea.php"; ?>
        <!--================End Instagram Area =================-->
<!--================ FIN DE CONTENIDO A PARTIR DE AQUI =================-->
    </div>
</section>
<br/>
<?php require_once "snippets/footer.php"; ?>
<!-- Optional JavaScript -->
<!--================ API KEY DE GOOGLE PARA QUE SE MUESTRE BIEN EL MAPA =================-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqoM9mfM_qjzk6jqCz1lXwcuftmK0hRPQ"></script>
<!--================ FIN API KEY =================-->
<script src="js/bootstrap.min.js"></script>
<script src="js/gmaps.min.js"></script>
<script src="js/google-map.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/mail-script.js"></script>
<script src="js/proyectoweb.js"></script>
<script src="js/contact.js"></script>

<script src="js/theme.js"></script>
<script>
    /*Script de Autoejecucion cuando cargue la pagina*/
    $(function () {
       //marcar menu con clase active, dara color ROJO
        $("#nav_contact").addClass("active");
    })
</script>
</body>
</html>