<?php
session_start();
require_once (dirname(__FILE__) . '/clases/conexion.php');
$c = new conectar();
?>
<!doctype html>
<html lang="es_PE" xmlns="http://www.w3.org/1999/html">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="img/favicon.png" type="image/png">
    <title>Ministerio Doulos - Id y hacer Discipulos a todas las naciones</title>
    <base href="<?php echo $c->urlbase; ?>">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendors/lightbox/simple-lightbox.min.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <link rel="stylesheet" href="vendors/popup/magnific-popup.css">

    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/my.css">
    <link rel="stylesheet" href="css/redessociales.css">
    <link rel="stylesheet" href="css/events.css">
    <!--Superslider-->
    <link rel="stylesheet" type="text/css" href="css/superslides.css" />
    <script src="js/jquery-3.3.1.min.js"></script>
    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/835ac9bcc2f5ac466178e6578/c48fd358b3ec665fb141d17c7.js");</script>

</head>
<body>
<?php include ("snippets/navbar.php"); ?>
<!--================Home Banner Area =================-->
<div id="index">
    <div id="webcontent" class="webcontent">
    <!--================ CONTENIDO A PARTIR DE AQUI =================-->
        <!--================ SuperSlider =================-->
        <section>
            <div id="slides">
                <ul class="slides-container">
                    <li>
                        <img src="img/superslider/1.jpg" alt="">
                        <div class="Description">
                            <h1>Doulos Ministery</h1>
                            <p>"Pensar demasiado te arruina. Arruina la situación, la da vueltas, te preocupa y
                                simplemente hace que todo sea mucho peor de lo que realmente es. Únete a nosotros! ”.</p>
                        </div>
                    </li>
                    <li>
                        <img src="img/superslider/2.jpg" alt="">
                        <div class="Description">
                            <h1>Doulos Ministery</h1>
                            <p>"Siempre recuerda que eres más valiente de lo que crees, más fuerte
                                de lo que pareces, y más inteligente de lo que piensas ”A. A. Milne.</p>
                        </div>
                    </li>
                    <li>
                        <img src="img/superslider/3.jpg" alt="">
                        <div class="Description">
                            <h1>Doulos Ministery</h1>
                            <p>"Aquellos que pueden imaginar cualquier cosa, pueden crear lo imposible".
                                - Alan Turing.</p>
                        </div>
                    </li>
                </ul>

                <nav class="slides-navigation">
                    <a href="#" class="prev"><i class="fa fa-chevron-circle-left fa-4x" aria-hidden="true"></i></a>
                    <a href="#" class="next"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true"></i></a>
                </nav>

            </div>
        </section>
        <!--================End SuperSlider =================-->
        <!--Sección Quienes Sómos-->
        <section id="about-us" class="home_blog_area p_100">
            <div class="home_blog_inner">
                <!--Sección Quienes Sómos-->
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <h4 class="title_item_h4_center">Quiénes Somos</h4>
                        <p class="text-center">En una sociedad  que intenta pervertir y anular los absolutos divinos y de respeto y honra hacia la familia, donde la sociedad ofrece el
                            engaño de la relatividad, nace este ministerio. Somos un grupo de adolescentes que buscan marcar el cambio en la sociedad, mediante actividades recreativas y de interacción
                            con nuestra comunidad.</p>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4">
                        <h5 class="title_item_h5 text-left">Misión</h5>
                        <p class="text-left">Mediante nuestras acciones queremos generar el cambio de nuestra sociedad.
                            Buscamos guiar a los jóvenes y adolescentes de nuestra sociedad por el camino
                            correcto.</p>
                    </div>
                    <div class="col-lg-4">
                        <h5 class="title_item_h5 text-right">Visión</h5>
                        <p class="text-right">Ser un grupo juvenil que crezca y guie a su vez a la juventud arequipeña,
                            influyendo en nuestra comunidad</p>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4">
                        <div class="h_blog_img imageGallery">
                            <img class="img-fluid lightbox" style="height: 80%; width: 80%; margin-left: auto; margin-right: auto; display: block;"
                                 src="img/mision-vision/1.jpg"  alt="1"
                                title="Misión">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="h_blog_img imageGallery">
                            <img class="img-fluid lightbox" style="height: 80%; width: 80%; margin-left: auto; margin-right: auto; display: block;"
                                 src="img/mision-vision/2.jpg" alt="2"
                                 title="Visión">
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
        </section>
        <!--Fin Sección Quienes Sómos-->
        <!--Sección Consejeria-->
        <section class="about_area pad_top">
            <div class="container">
                <div class="about_inner row">
                    <div class="col-lg-6">
                        <div class="about_text">
                            <h3>Consejería <br />Familiar</h3>
                            <h5>Nos complace ofrecer servicios de consejería para jóvenes y familias en general</h5>
                            <p>Estamos aqui para escuchar tu consulta, escríbenos aquí para realizar tu consulta.</p>
                            <br/>
                            <a href="contact.php" class="genric-btn info ">Info</a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about_img"><img class="img-fluid" src="img/about-1.jpg" alt=""></div>
                    </div>
                </div>
            </div>
        </section>
        <!--Sección Consejeria-->
        <!--Sección Proyectos-->
        <section id="our-projects" class="home_blog_area p_100">
            <div class="home_blog_inner">
                <div class="row">
                    <div class="col-lg-2 col-sm-2 col-xs-2"></div>
                    <div class="col-lg-8 col-sm-8 col-xs-8">
                        <h4 class="title_item_h4_center mb-5">Nuestros Proyectos</h4>
                    </div>
                    <div class="col-lg-2 col-sm-2 col-xs-2"></div>
                </div>
                <div class="row h-100">
                    <div class="col-lg-2 col-sm-2 col-xs-2"></div>
                    <div class="col-lg-8">
                        <div class="projects_slider_inner">
                            <div class="projects_slider owl-carousel">
                                <div class="item">
                                    <div class="media">
                                        <div class="d-flex">
                                            <img src="img/projects/projects-1.jpg" alt="...">
                                        </div>
                                        <div class="media-body">
                                            <p>Despidiendo el 2019 de la mejor manera posible para los niños necesitados de Canteras - Arequipa.</p>
                                            <h4>Apoyando en Canteras!</h4>
                                            <h5>Brindando esperanza a los demás</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="media">
                                        <div class="d-flex">
                                            <img src="img/projects/projects-3.jpg" alt="">
                                        </div>
                                        <div class="media-body">
                                            <p>Apóyanos en acoplar útiles escolares para niños de mayor necesidad en Arequipa!</p>
                                            <h4>Ministerio Flecha Internacional</h4>
                                            <h5>Obra naciente</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="media">
                                        <div class="d-flex">
                                            <img src="img/projects/projects-2.jpg" alt="">
                                        </div>
                                        <div class="media-body">
                                            <p>Feliz 28 de Julio</p>
                                            <h4>Nuestros preparativos por la independencia de nuestra patria, ¡Felices Fiestas!</h4>
                                            <h5>Día de nuestra Patria</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-2 col-xs-2"></div>
                </div>
            </div>
        </section>
        <!--Fin Sección Proyectos-->
        <!--Sección Testimonios-->
        <section id="testimonies" class="home_blog_area p_100">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="title_item_h4_center">Testimonios</h4>
                </div>
            </div>
            <div class="container">
                <!-- condoritos e infos -->
                <div class="tab-content">
                    <!-- elemento -->
                    <div class="tab-pane active" id="home" role="tabpanel">
                        <div class="row align-items-center ">
                            <div class="col-md-4 mb-md-3 mb-lg-0">
                                <img src="img/testimonials/lg_1.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="col-md-8 mb-md-5">
                                <h4 class="lead">Frase de Persona</h4>
                                <h2><b>Name</b></h2>
                                <p>Cual es su experiencia al reunirse con jovenes, como se siente al estar con ellos</p>
                            </div>
                        </div>
                    </div><!-- fin elemento -->

                    <!-- elemento -->
                    <div class="tab-pane" id="home2" role="tabpanel">
                        <div class="row align-items-center ">
                            <div class="col-md-4 mb-md-3 mb-lg-0">
                                <img src="img/testimonials/lg_2.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="col-md-8 mb-md-5">
                                <h4 class="lead">Frase de Persona</h4>
                                <h2><b>Name</b></h2>
                                <p>Cual es su experiencia al reunirse con jovenes, como se siente al estar con ellos</p>
                            </div>
                        </div>
                    </div><!-- fin elemento -->

                    <!-- elemento -->
                    <div class="tab-pane" id="home3" role="tabpanel">
                        <div class="row align-items-center ">
                            <div class="col-md-4 mb-md-3 mb-lg-0">
                                <img src="img/testimonials/lg_3.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="col-md-8 mb-md-5">
                                <h4 class="lead">Frase de Persona</h4>
                                <h2><b>Name</b></h2>
                                <p>Cual es su experiencia al reunirse con jovenes, como se siente al estar con ellos</p>
                            </div>
                        </div>
                    </div><!-- fin elemento -->


                </div><!-- fin condoritos e infos -->
                <!-- nav -->
                <div class="row">
                    <div class="offset-lg-4 col-lg-8">
                        <ul class="nav nav-tabs caritas" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home" role="tab">
                                    <img src="img/testimonials/sm_1.jpg" alt="">
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#home2" role="tab">
                                    <img src="img/testimonials/sm_2.jpg" alt="">
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#home3" role="tab">
                                    <img src="img/testimonials/sm_3.jpg" alt="">
                                </a>
                            </li>
                        </ul><!-- fin nav -->
                    </div>
                </div>
            </div>
        </section>
        <!--Fin Sección Testimonios-->
        <!--Sección Eventos-->
        <section id="events" class="home_blog_area pb-4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8 text-center">
                        <h4 class="title_item_h4_center">Nuestros Eventos</h4>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
                <div class="blog-card">
                    <div class="meta">
                        <div class="photo" style="background-image: url(https://storage.googleapis.com/chydlx/codepen/blog-cards/image-1.jpg)"></div>
                        <ul class="details">
                            <li class="author"><a href="#">John Doe</a></li>
                            <li class="date">Aug. 24, 2015</li>
                            <li class="tags">
                                <ul>
                                    <li><a href="#">Learn</a></li>
                                    <li><a href="#">Code</a></li>
                                    <li><a href="#">HTML</a></li>
                                    <li><a href="#">CSS</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="description">
                        <h1>Learning to Code</h1>
                        <h2>Opening a door to the future</h2>
                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis! Neque ad aliquam facilis numquam. Veritatis, sit.</p>
                        <p class="read-more">
                            <a href="#">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="blog-card alt">
                    <div class="meta">
                        <div class="photo" style="background-image: url(https://storage.googleapis.com/chydlx/codepen/blog-cards/image-2.jpg)"></div>
                        <ul class="details">
                            <li class="author"><a href="#">Jane Doe</a></li>
                            <li class="date">July. 15, 2015</li>
                            <li class="tags">
                                <ul>
                                    <li><a href="#">Learn</a></li>
                                    <li><a href="#">Code</a></li>
                                    <li><a href="#">JavaScript</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="description">
                        <h1>Mastering the Language</h1>
                        <h2>Java is not the same as JavaScript</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis! Neque ad aliquam facilis numquam. Veritatis, sit.</p>
                        <p class="read-more">
                            <a href="#">Read More</a>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!--Sección Eventos-->
        <!--================Instagram Area =================-->
        <?php require_once "snippets/InstaArea.php"; ?>
        <!--================End Instagram Area =================-->
    <!--================ FIN DE CONTENIDO =================-->
    </div>
</div>

<?php require_once "snippets/footer.php"; ?>

<!-- Optional JavaScript -->

<script src="js/bootstrap.min.js"></script>
<script src="vendors/superslider/jquery.superslides.min.js"></script>
<script src="vendors/lightbox/simple-lightbox.jquery.min.js"></script>
<script src="vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="vendors/popup/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="vendors/counter-up/jquery.waypoints.min.js"></script>
<script src="vendors/counter-up/jquery.counterup.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/proyectoweb.js"></script>
<script src="js/theme.js"></script>
<script>
    /*Script de Autoejecucion cuando cargue la pagina*/
    $(function () {
        $('#slides').superslides({
            hashchange: true,
            play: 5000,
              animation: 'fade',
            pagination: true,
        });

    });
</script>

</body>
</html>