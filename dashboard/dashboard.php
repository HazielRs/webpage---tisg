<?php include ('procesos/security-login.php');
require_once (dirname(__FILE__) . "/../clases/conexion.php");
require_once (dirname(__FILE__) . "/../clases/usuarios.php");
$obj=new usuarios();
$datos=$obj->TablaDatosUsuario();
?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Ministerio DOULOS - Administración">
  <title>Ventana de Administración</title>
  <!-- Favicon -->
  <link rel="icon" href="assets/img/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Page plugins -->
  <link rel="stylesheet" href="assets/vendor/fullcalendar/dist/fullcalendar.min.css">
  <link rel="stylesheet" href="assets/vendor/sweetalert2/dist/sweetalert2.min.css">
  <link rel="stylesheet" href="assets/vendor/animate.css/animate.min.css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body>
  <?php require_once ('Sidenav.php')?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <?php require_once ('Topnav.php')?>
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-default pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Ventana Principal</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="../index.php"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Main</a></li>

                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <button type="button" class="btn btn-sm btn-icon bg-gradient-white" data-toggle="modal" data-target="#modal-default">
                <span class="btn-inner--icon"><i class="ni ni-email-83"></i></span>
              </button>
                <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="modal-title" id="modal-title-default">Mensaje</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <label class="form-control-label" for="mail-usuario">Destinatario</label>
                                        <input type="email" class="form-control form-control-sm" name="mail-usuario" id="mail-usuario" placeholder="nombre@ejemplo.com">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label" for="msg-title">Destinatario</label>
                                        <input type="email" class="form-control" name="msg-title" id="msg-title" placeholder="Titulo del mensaje.">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label" for="msg-body">Mensaje</label>
                                        <textarea name="msg-body" class="form-control" id="msg-body" rows="5" placeholder="Mensaje breve y conciso :)"></textarea>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary">Guardar</button>
                                <button type="button" class="btn btn-danger  ml-auto" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <!-- Bienvenida -->
          <div class="row">
            <div class="col-xl-12 col-md-12">
              <div class="card bg-translucent-primary"> <!--Blue-->
                <div class="card-body">
                  <h3 class="card-title display-4 text-white">Bienvenid@!</h3>
                  <blockquote class="blockquote text-white mb-0">
                    <p>"Cómo pasamos nuestros días es, por supuesto, cómo pasamos nuestras vidas".</p>
                    <footer class="blockquote-footer text-info"><cite title="Source Title">Annie Dillard</cite></footer>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-4 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">N° Posts</h5>
                      <span class="h1 font-weight-bold mb-0">0</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-purple text-white rounded-circle shadow">
                        <i class="ni ni-collection"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Nuevos Usuarios</h5>
                      <span class="h1 font-weight-bold mb-0">0</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-blue text-white rounded-circle shadow">
                        <i class="ni ni-badge"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-12">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">N° Visitas</h5>
                      <span class="h1 font-weight-bold mb-0">0</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                        <i class="ni ni-chart-bar-32"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6 bg-lighter">
      <div class="row">
        <div class="col-xl-8">
          <div class="card bg-gradient-blue">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-light text-uppercase ls-1 mb-1">Panorama General</h6>
                  <h5 class="h3 text-white mb-0">Número de Visitas</h5>
                </div>
<!--                <div class="col">-->
<!--                  <ul class="nav nav-pills justify-content-end">-->
<!--                    <li class="nav-item mr-2 mr-md-0" data-toggle="chart" data-target="#chart-bars" data-update='{"data":{"datasets":[{"data":[0, 20, 10, 30, 15, 40, 20, 60, 60]}]}}' data-prefix="" data-suffix="">-->
<!--                      <a href="#" class="nav-link py-2 px-3 active" data-toggle="tab">-->
<!--                        <span class="d-none d-md-block">Month</span>-->
<!--                        <span class="d-md-none">M</span>-->
<!--                      </a>-->
<!--                    </li>-->
<!--                    <li class="nav-item" data-toggle="chart" data-target="#chart-bars" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="" data-suffix="">-->
<!--                      <a href="#" class="nav-link py-2 px-3" data-toggle="tab">-->
<!--                        <span class="d-none d-md-block">Week</span>-->
<!--                        <span class="d-md-none">W</span>-->
<!--                      </a>-->
<!--                    </li>-->
<!--                  </ul>-->
<!--                </div>-->
              </div>
            </div>
            <div class="card-body">
              <!-- Chart -->
              <div class="chart">
                <!-- Chart wrapper -->
                <canvas id="chart-bars" class="chart-canvas"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <!-- Title -->
              <h5 class="h3 mb-0">Administradores</h5>
            </div>
            <!-- Card search -->
            <div class="card-header py-0">
              <!-- Search form -->
              <form>
                <div class="form-group mb-0">
                  <div class="input-group input-group-lg input-group-flush">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <span class="fas fa-search"></span>
                      </div>
                    </div>
                      <input type="search" class="form-control" placeholder="Búsqueda">
                  </div>
                </div>
              </form>
            </div>
            <!-- Card body -->
            <div class="card-body">
              <!-- List group -->
              <ul class="list-group list-group-flush list my--3">
                <li class="list-group-item px-0">
                  <div class="row align-items-center">
                    <div class="col-auto">
                      <!-- Avatar -->
                      <i class="fas fa-user-alt"></i>
                    </div>
                    <div class="col ml--2">
                      <h4 class="mb-0">
                        <a href="users.php">Admin1</a>
                      </h4>
                      <span class="text-success">●</span>
                      <small>Activo</small>
                    </div>
                    <div class="col-auto">
                      <button type="button" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i> </button>
                    </div>
                  </div>
                </li>
                <li class="list-group-item px-0">
                  <div class="row align-items-center">
                    <div class="col-auto">
                      <!-- Avatar -->
                      <i class="fas fa-user-alt"></i>
                    </div>
                    <div class="col ml--2">
                      <h4 class="mb-0">
                        <a href="users.php">Admin2</a>
                      </h4>
                      <span class="text-warning">●</span>
                      <small>Oculto</small>
                    </div>
                    <div class="col-auto">
                        <button type="button" class="btn btn-sm btn-success"><i class="fas fa-check"></i> </button>
                    </div>
                  </div>
                </li>
                <li class="list-group-item px-0">
                  <div class="row align-items-center">
                    <div class="col-auto">
                      <!-- Avatar -->
                      <i class="fas fa-user-alt"></i>
                    </div>
                    <div class="col ml--2">
                      <h4 class="mb-0">
                        <a href="users.php">Admin3</a>
                      </h4>
                      <span class="text-danger">●</span>
                      <small>Oculto</small>
                    </div>
                    <div class="col-auto">
                      <button type="button" class="btn btn-sm btn-success"><i class="fas fa-check"></i> </button>
                    </div>
                  </div>
                </li>
                  <li class="list-group-item px-0">
                      <div class="row align-items-center">
                          <div class="col-auto">
                              <!-- Avatar -->
                              <i class="fas fa-user-alt"></i>
                          </div>
                          <div class="col ml--2">
                              <h4 class="mb-0">
                                  <a href="users.php">Admin4</a>
                              </h4>
                              <span class="text-danger">●</span>
                              <small>Oculto</small>
                          </div>
                          <div class="col-auto">
                              <button type="button" class="btn btn-sm btn-success"><i class="fas fa-check"></i> </button>
                          </div>
                      </div>
                  </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-5">
          <!-- Messages -->
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <!-- Title -->
              <h5 class="h3 mb-0">Últimos Mensajes</h5>
            </div>
            <!-- Card body -->
            <div class="card-body p-0">
              <!-- List group -->
              <div class="list-group list-group-flush">
                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start py-4 px-4">
                  <div class="d-flex w-100 justify-content-between">
                    <div>
                      <div class="d-flex w-100 align-items-center">
                        <i class="fas fa-user-friends avatar avatar-xs mr-2"></i>
                        <h5 class="mb-1">Usuario</h5>
                      </div>
                    </div>
                  </div>
                  <h4 class="mt-3 mb-1"> Titulo de mensaje 1</h4>
                  <p class="text-sm mb-0">Mensaje, este estara en un modal (pop-up) para poder escribirlo/visualizarlo. Posible limite de palabras</p>
                </a>
                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start ">
                  <div class="d-flex w-100 justify-content-between">
                    <div>
                      <div class="d-flex w-100 align-items-center">
                          <i class="fas fa-user-friends avatar avatar-xs mr-2"></i>
                        <h5 class="mb-1">Mike</h5>
                      </div>
                    </div>
                  </div>
                  <h4 class="mt-3 mb-1"><span class="text-info">●</span> Titulo de mensaje 2</h4>
                  <p class="text-sm mb-0">Mensaje, este estara en un modal (pop-up) para poder escribirlo/visualizarlo. Posible limite de palabras</p>
                </a>
                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start py-4 px-4">
                  <div class="d-flex w-100 justify-content-between">
                    <div>
                      <div class="d-flex w-100 align-items-center">
                          <i class="fas fa-user-friends avatar avatar-xs mr-2"></i>
                        <h5 class="mb-1">Tim</h5>
                      </div>
                    </div>
                  </div>
                  <h4 class="mt-3 mb-1"> Titulo de mensaje 3</h4>
                  <p class="text-sm mb-0">Mensaje, este estara en un modal (pop-up) para poder escribirlo/visualizarlo. Posible limite de palabras</p>
                </a>
              </div>
            </div>
            <!-- Card footer -->
            <div class="card-footer py-4">
              <nav aria-label="...">
                <ul class="pagination pagination-sm justify-content-end mb-0">
                  <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">
                      <i class="fas fa-angle-left"></i>
                      <span class="sr-only">Previous</span>
                    </a>
                  </li>
                  <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="#">2</a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="#">3</a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="#">
                      <i class="fas fa-angle-right"></i>
                      <span class="sr-only">Next</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <div class="col-xl-7">
          <!-- Calendar widget -->
          <!--* Card header *-->
          <!--* Card body *-->
          <!--* Card init *-->
          <div class="card card-calendar">
            <!-- Card header -->
            <div class="card-header">
              <div class="row">
                <div class="col-lg-4">
                  <div id="calendar-year" class="h5 text-muted mb-1 "></div>
                  <div id="calendar-day" class="h3 mb-0"></div>
                </div>
                <div class="col-lg-8 mt-3 mt-lg-0 text-lg-right">
                  <a href="#" class="fullcalendar-btn-prev btn btn-sm btn-neutral">
                    <i class="fas fa-angle-left"></i>
                  </a>
                  <a href="#" class="fullcalendar-btn-next btn btn-sm btn-neutral">
                    <i class="fas fa-angle-right"></i>
                  </a>
                  <a href="#" class="btn btn-sm btn-neutral" data-calendar-view="month">Mes</a>
                  <a href="#" class="btn btn-sm btn-neutral" data-calendar-view="basicWeek">Semana</a>
                  <a href="#" class="btn btn-sm btn-neutral" data-calendar-view="basicDay">Día</a>
                </div>
              </div>

            </div>
            <!-- Card body -->
            <div class="card-body">
              <div data-toggle="calendar" class="calendar" id="calendar"></div>
            </div>
          </div>
          <!-- Modal - Add new event -->
          <!--* Modal header *-->
          <!--* Modal body *-->
          <!--* Modal footer *-->
          <!--* Modal init *-->
            <div class="modal fade" id="new-event" tabindex="-1" role="dialog" aria-labelledby="new-event-label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-secondary" role="document">
                    <div class="modal-content">
                        <!-- Modal body -->
                        <div class="modal-body">
                            <form class="" id="new-event--form">
                                <div class="form-group">
                                    <label class="form-control-label">Titulo de Evento</label>
                                    <input type="text" id="new-event--title" class="form-control form-control-alternative" placeholder="Titulo de Evento" value="">
                                </div>
                                <div class="form-group mb-0">
                                    <label class="form-control-label d-block mb-3">Color</label>
                                    <div class="btn-group btn-group-toggle btn-group-colors event-tag" data-toggle="buttons">
                                        <label class="btn bg-info active"><input type="radio" name="event-tag" value="bg-info" autocomplete="off" checked></label>
                                        <label class="btn bg-warning"><input type="radio" name="event-tag" value="bg-warning" autocomplete="off"></label>
                                        <label class="btn bg-danger"><input type="radio" name="event-tag" value="bg-danger" autocomplete="off"></label>
                                        <label class="btn bg-success"><input type="radio" name="event-tag" value="bg-success" autocomplete="off"></label>
                                        <label class="btn bg-default"><input type="radio" name="event-tag" value="bg-default" autocomplete="off"></label>
                                        <label class="btn bg-primary"><input type="radio" name="event-tag" value="bg-primary" autocomplete="off"></label>
                                    </div>
                                </div>
                                <input id="new-event--start" type="hidden" class="" />
                                <input id="new-event--end" type="hidden" class="" />
                                <div class="form-group">
                                    <label class="form-control-label">Descripción</label>
                                    <textarea id="new-event--description" class="form-control form-control-alternative textarea-autosize" placeholder="Descripción de Evento"></textarea>
                                    <i class="form-group--bar"></i>
                                </div>
                                <input id="edit-event--id" type="hidden" class="">
                            </form>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button id="new-event--add" type="submit" class="btn btn-primary">Agregar</button>
                            <button type="button" class="btn btn-outline-danger ml-auto" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
          <!-- Modal - Edit event -->
          <!--* Modal body *-->
          <!--* Modal footer *-->
          <!--* Modal init *-->
            <div class="modal fade" id="edit-event" tabindex="-1" role="dialog" aria-labelledby="edit-event-label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-secondary" role="document">
                    <div class="modal-content">
                        <!-- Modal body -->
                        <div class="modal-body">
                            <form class="edit-event--form" id="evento-cal">
                                <div class="form-group">
                                    <label class="form-control-label">Titulo de Evento</label>
                                    <input id="edit-event--title" name="title-event" type="text" class="form-control form-control-alternative " placeholder="Titulo de Evento">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label d-block mb-3">Color</label>
                                    <div class="btn-group btn-group-toggle btn-group-colors edit-event-tag mb-0" data-toggle="buttons">
                                        <label class="btn bg-info active"><input type="radio" name="event-tag" value="bg-info" autocomplete="off" checked></label>
                                        <label class="btn bg-warning"><input type="radio" name="event-tag" value="bg-warning" autocomplete="off"></label>
                                        <label class="btn bg-danger"><input type="radio" name="event-tag" value="bg-danger" autocomplete="off"></label>
                                        <label class="btn bg-success"><input type="radio" name="event-tag" value="bg-success" autocomplete="off"></label>
                                        <label class="btn bg-default"><input type="radio" name="event-tag" value="bg-default" autocomplete="off"></label>
                                        <label class="btn bg-primary"><input type="radio" name="event-tag" value="bg-primary" autocomplete="off"></label>
                                    </div>
                                </div>
                                <input id="edit-event--start" type="hidden" class="" />
                                <input id="edit-event--end" type="hidden" class="" />
                                <div class="form-group">
                                    <label class="form-control-label">Descripción</label>
                                    <textarea id="edit-event--description" class="form-control form-control-alternative textarea-autosize" placeholder="Descripción de Evento"></textarea>
                                    <i class="form-group--bar"></i>
                                </div>
                                <input id="edit-event--id" type="hidden" class="">
                            </form>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button class="btn btn-primary" data-calendar="update">Actualizar</button>
                            <button class="btn btn-danger" data-calendar="delete">Eliminar</button>
                            <button class="btn btn-link ml-auto" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Optional JS -->
  <script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
  <script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>
  <script src="assets/vendor/moment/min/moment.min.js"></script>
  <script src="assets/vendor/fullcalendar/dist/fullcalendar.min.js"></script>
  <script src="assets/vendor/fullcalendar/dist/locale/es.js"></script>
  <script src="assets/vendor/moment/locale/es.js"></script>
  <script src="assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>
  <!-- Argon JS -->
  <script src="assets/js/argon.js?v=1.2.0"></script>
    <script>
        $(document).ready(function() {
            $(".nav li.disabled a").click(function() {
                return false;
            });
        });
    </script>
  <script>
      function edit(event){
          start = event.start.format('YYYY-MM-DD');
          if(event.end){
              end = event.end.format('YYYY-MM-DD');
          }else{
              end = start;
          }

          id =  event.id;

          Event = [];
          Event[0] = id;
          Event[1] = start;
          Event[2] = end;

          $.ajax({
              url: 'procesos/editEventDate.php',
              type: "POST",
              data: {Event:Event},
              success: function(rep) {
                  if(rep == 'OK'){
                      notify('Exito', 'El evento se ha guardado correctamente!', 'success');
                  }else{
                      notify('Error', 'No se pudo guardar. Inténtalo de nuevo.', 'danger');

                  }
              }
          });
      }
  </script>
</body>

</html>

