<?php include ('procesos/security-login.php') ?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Ministerio DOULOS - Administración">
    <title>Tus Mensajes</title>
    <!-- Favicon -->
    <link rel="icon" href="assets/img/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <!-- Page plugins -->
    <link rel="stylesheet" href="assets/vendor/sweetalert2/dist/sweetalert2.min.css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">
</head>
<body>
<?php require_once ('Sidenav.php')?>
<!-- Main content -->
<div class="main-content" id="panel">
    <?php require_once ('Topnav.php')?>
    <!-- Header -->
    <div class="header bg-default pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-12">
                        <h6 class="h2 text-white d-inline-block mb-0">Mensajes</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="../index.php"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="dashboard.php">Main</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Mensajes</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Core -->
    <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="assets/js/argon.js?v=1.2.0"></script>

</body>
<!--<button type="button" class="btn btn-block btn-default" data-toggle="modal" data-target="#modal-form">Form</button>-->
<!--<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">-->
<!--    <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">-->
<!--        <div class="modal-content">-->
<!--            <div class="modal-body p-0">-->
<!--                <div class="card bg-secondary border-0 mb-0">-->
<!--                    <div class="card-header bg-transparent pb-5">-->
<!--                        <div class="text-muted text-center mt-2 mb-3"><small>Sign in with</small></div>-->
<!--                        <div class="btn-wrapper text-center">-->
<!--                            <a href="#" class="btn btn-neutral btn-icon">-->
<!--                                <span class="btn-inner--icon"><img src="assets/img/icons/common/github.svg"></span>-->
<!--                                <span class="btn-inner--text">Github</span>-->
<!--                            </a>-->
<!--                            <a href="#" class="btn btn-neutral btn-icon">-->
<!--                                <span class="btn-inner--icon"><img src="assets/img/icons/common/google.svg"></span>-->
<!--                                <span class="btn-inner--text">Google</span>-->
<!--                            </a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="card-body px-lg-5 py-lg-5">-->
<!--                        <div class="text-center text-muted mb-4">-->
<!--                            <small>Or sign in with credentials</small>-->
<!--                        </div>-->
<!--                        <form role="form">-->
<!--                            <div class="form-group mb-3">-->
<!--                                <div class="input-group input-group-merge input-group-alternative">-->
<!--                                    <div class="input-group-prepend">-->
<!--                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>-->
<!--                                    </div>-->
<!--                                    <input class="form-control" placeholder="Email" type="email">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <div class="input-group input-group-merge input-group-alternative">-->
<!--                                    <div class="input-group-prepend">-->
<!--                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>-->
<!--                                    </div>-->
<!--                                    <input class="form-control" placeholder="Password" type="password">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="custom-control custom-control-alternative custom-checkbox">-->
<!--                                <input class="custom-control-input" id=" customCheckLogin" type="checkbox">-->
<!--                                <label class="custom-control-label" for=" customCheckLogin">-->
<!--                                    <span class="text-muted">Remember me</span>-->
<!--                                </label>-->
<!--                            </div>-->
<!--                            <div class="text-center">-->
<!--                                <button type="button" class="btn btn-primary my-4">Sign in</button>-->
<!--                            </div>-->
<!--                        </form>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->