<?php include ('procesos/security-login.php');
require_once (dirname(__FILE__) . "/../clases/conexion.php");
require_once (dirname(__FILE__) . "/../clases/usuarios.php");
$obj= new usuarios();
//$_SESSION['iduser'] esto tiene IdUser
$datos = $obj->TablaDatosUsuario();
//var_dump($user);
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <!-- <base href="http://coar/dashboard/">-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Ministerio DOULOS - Administración">
    <title>Usuarios</title>
    <!-- Favicon -->
    <link rel="icon" href="assets/img/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <!-- Page plugins -->
    <link rel="stylesheet" href="assets/vendor/sweetalert2/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="assets/vendor/animate.css/animate.min.css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">

    <script src="assets/js/validar-users.js"></script>
</head>

<body>
<?php require_once ('Sidenav.php')?>
<!-- Main content -->
<div class="main-content" id="panel">
    <?php require_once ('Topnav.php')?>
    <!-- Header -->
    <div class="header bg-default pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-12">
                        <h6 class="h2 text-white d-inline-block mb-0">Usuarios</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="../index.php"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="dashboard.php">Main</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Usuarios</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt--6">
        <!-- Table -->
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <div class="row">
                            <div class="col-12">
                                <h3 class="mb-0">Tabla de Usuarios</h3>
                                <p class="text-sm mb-0">
                                    Aquí podrás encontrar a todos los usuarios/administradores de Ministerio DOULOS.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Light table -->
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush table-hover">
                            <thead class="thead-light">
                            <tr>
                                <th>Username</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Correo Electrónico</th>
                                <th>Teléfono</th>
                                <th>Status</th>
                                <th>Rol</th>
                                <th>Acción</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach ($datos as $row) {
                                //var_dump($row);
                                ?>
                            <tr>
                                <td><?php echo $row['username']?></td>
                                <td><?php echo $row['nombre']?></td>
                                <td><?php echo $row['apellido']?></td>
                                <td><?php echo $row['email']?></td>
                                <td><?php echo $row['telefono']?></td>
                                <td>
                                    <span class="badge badge-dot mr-4">
                                        <i class="<?php if($row['activo']=="1") echo "bg-success"; else echo "bg-warning"?>"></i>
                                    <span class="status"><?php if($row['activo']=="1") echo "Activo"; else echo "Desactivo"?></span>
                                </td>
                                <td>
                                    <span class="badge badge-dot mr-4">
                                        <i class="<?php if($row['isadministrador']=="1") echo "bg-success"; else echo "bg-info"?>"></i>
                                    <span class="status"><?php if($row['isadministrador']=="1") echo "Administrador"; else echo "Usuario"?></span>
                                </td>
                                <td class="table-actions">
                                    <button name="btn-modal" class="btn btn-sm btn-primary" type="button" data-toggle="modal" data-iduser="<?php echo $row['id_usuario'] ?>"> <!--data-target="#modal-default"-->
                                        <span class="btn-inner--icon" data-toggle="tooltip" data-original-title="Editar"><i class="fas fa-user-edit"></i></span>
                                    </button>
                                    <button name="delete-user" class="btn btn-sm btn-outline-danger" type="button" data-iduser-delete="<?php echo $row['id_usuario'] ?>">
                                        <span class="btn-inner--icon" data-toggle="tooltip" data-original-title="Eliminar"><i class="fas fa-trash"></i></span>
                                    </button>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Username</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Correo Electrónico</th>
                                <th>Teléfono</th>
                                <th>Status</th>
                                <th>Rol</th>
                                <th>Acción</th>
                            </tr>
                            </tfoot>
                        </table>

                        <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                            <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h6 class="modal-title" id="modal-title-default"></h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="user-edit">
                                            <input type="hidden" id="id_modal">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="user-name">Nombre</label>
                                                        <input type="text" class="form-control" name="user-name"
                                                               id="user_name" placeholder="Nombre de usuario">
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="user-surname">Apellido</label>
                                                        <input type="text" class="form-control" name="user-surname"
                                                               id="user_surname" placeholder="Apellido del usuario">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="user-email">Correo Electrónico</label>
                                                        <input type="text" class="form-control form-control-sm"
                                                               name="user-email" id="user_email"
                                                               placeholder="Correo electrónico del usuario" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-9">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="user-role">Rol</label>
                                                        <select class="form-control" id="user_role" name="user-role">
                                                            <option value="U">Usuario</option>
                                                            <option value="A">Administrador</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="user-status">Estado</label>
                                                        <label class="custom-toggle custom-toggle-success">
                                                            <input type="checkbox" id="user_status">
                                                            <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="user-number">Teléfono</label>
                                                        <input type="tel" class="form-control"
                                                               name="user-number" id="user_number"
                                                               placeholder="Teléfono del usuario">
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="user-password">Password</label>
                                                        <input type="text" class="form-control" value=""
                                                               name="user-password" id="user_password"
                                                               placeholder="Contraseña del usuario">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-primary" id="btn_saveuser">Guardar Cambios</button>
                                        <button type="button" class="btn btn-outline-danger  ml-auto" data-dismiss="modal">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Argon Scripts -->
<!-- Core -->
<script src="assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/js-cookie/js.cookie.js"></script>
<script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
<script src="assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>
    <script src="assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

<!-- Argon JS -->
<script src="assets/js/argon.js?v=1.2.0"></script>

    <script>
        //Guardar cambios en usuario
        $('#btn_saveuser').click(function(){
            datos=JSON.stringify({ 'id_usuario': document.getElementById("id_modal").value,
                    'nombre': document.getElementById("user_name").value,
                    'isadministrador': document.getElementById("user_role").value,
                    'activo': document.getElementById("user_status").checked,
                    'apellido': document.getElementById("user_surname").value,
                    'telefono': document.getElementById("user_number").value,
                    'password': document.getElementById("user_password").value,
                });

            if (validar() === false)
                return false;
            else {
                $.ajax({
                    type:"POST",
                    data:datos,
                    url:"procesos/saveuser.php",
                    success:function(r){
                        r=JSON.parse(r);
                        console.log(r);

                        if(r.result==true){
                            Swal({
                                title: r.title,
                                text: r.message,
                                type:'success',
                                confirmButtonText: 'OK'
                            })
                                .then((value) => {
                                    location.reload();
                                });
                            $("#modal-default").modal('hide');
                        }
                        else
                            Swal.fire(r.title,r.message,'error');
                    }
                });
            }
        });
        //Traer info del usuario
        $("button[name='btn-modal']").click(function() {
        //$('.btn-modal').click(function () {
            var id = this.getAttribute('data-iduser');
            $.ajax({
                type:"POST",
                data:JSON.stringify({ 'iduser': id }),
                dataType: "json",
                url:"procesos/trae-info-usuario.php",
                success:function(r){
                    console.log(r);
                    if(r.result){
                        document.getElementById('modal-title-default').innerHTML = r.datos.username + ': Edición.';
                        document.getElementById('id_modal').value=r.datos.id_usuario;
                        document.getElementById('user_name').value=r.datos.nombre;
                        document.getElementById('user_surname').value=r.datos.apellido;

                        if (r.datos.activo=="1")
                            document.getElementById('user_status').checked=true;
                        else
                            document.getElementById('user_status').checked=false;

                        document.getElementById('user_email').value=r.datos.email;

                        if (r.datos.isadministrador=="1")
                            document.getElementById('user_role').value="A";
                        else
                            document.getElementById('user_role').value="U";

                        document.getElementById('user_number').value=r.datos.telefono;
                        document.getElementById('user_password').value="";

                        $("#modal-default").modal({ backdrop: "static" });
                    }else{
                         Swal.fire('Error en Base de datos', 'No se pudo actualizar', "error");
                    }
                }
            });

        });
        //eliminar usuario
        $("button[name='delete-user']").click(function() {

            let id = this.getAttribute('data-iduser-delete');

            swal({
                title: 'Estás Seguro?',
                text: "No podrás revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-danger',
                confirmButtonText: 'Si, Eliminar',
                cancelButtonClass: 'btn btn-secondary',
                cancelButtonText: 'Cancelar',
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type:"POST",
                        data:JSON.stringify({ 'id_usuario': id }),
                        dataType: "json",
                        url: 'procesos/deleteuser.php',
                        success:function (r) {
                            console.log(r);
                            if (r.result){
                                swal({
                                    title: r.title,
                                    text: r.message,
                                    type: 'success',
                                    buttonsStyling: false,
                                    confirmButtonClass: 'btn btn-primary'
                                })
                                    .then((value) => {
                                        location.reload();
                                    });
                            }
                            else swal(r.title, r.message, "error");
                        }

                    });

                }
            })
        });
    </script>
</body>

</html>