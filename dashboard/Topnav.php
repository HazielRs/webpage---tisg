<?php
// include ('login_form/security-login.php');

require_once (dirname(__FILE__) . "/../clases/conexion.php");
require_once (dirname(__FILE__) . "/../clases/usuarios.php");
$obj= new usuarios();
//$_SESSION['iduser'] esto tiene IdUser
$user = $obj->obtenDatosUsuario($_SESSION['iduser']);
//var_dump($user);
?>
<!-- Topnav -->
<nav class="navbar navbar-top navbar-expand navbar-dark bg-default border-bottom">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Navbar links -->
            <ul class="navbar-nav align-items-center ml-md-auto">
                <li class="nav-item d-xl-none">
                    <!-- Sidenav toggler -->
                    <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav align-items-center ml-md-auto">
                <li class="nav-item">
                    <a class="nav-link" href="../contact.php">
                        <span class="nav-link-text d-none d-lg-block text-sm">Contacto</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../projects.php">
                        <span class="nav-link-text d-none d-lg-block text-sm">Proyectos</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../events.php">
                        <span class="nav-link-text d-none d-lg-block text-sm">Eventos</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../blog.php">
                        <span class="nav-link-text d-none d-lg-block text-sm">Blog</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../archives.php">
                        <span class="nav-link-text d-none d-lg-block text-sm">Archivos</span>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav align-items-center ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                            <div class="media-body ml-2 d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold">
                                    <?php echo $user['nombre'] . ' ' . $user['apellido'];?>
                                </span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Bienvenid@!</h6>
                        </div>
                        <a href="profile.php" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>Mi Perfil</span>
                        </a>
                        <a href="calendar.php" class="dropdown-item">
                            <i class="ni ni-calendar-grid-58"></i>
                            <span>Actividades</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="logout.php" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Cerrar Sesión</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
