<?php include ('procesos/security-login.php');
require_once (dirname(__FILE__) . "/../clases/conexion.php");
require_once (dirname(__FILE__) . "/../clases/articulos.php");

$id = -1;
if(isset($_GET['id']))
    $id= $_GET['id'];

$obj=new articulos();
$estaPublicado=false;
if($id>0) {
    $datos = $obj->obtenDatosArt($id);
    $estaPublicado = $datos['FechaPublicacion_Art']!=null;
    //if(intval(date("Y",strtotime($datos['')) )>1900)
}
else{
    $datos = $obj->RegistroVacio();
}

//var_dump($datos);
?>
    <!DOCTYPE html>

    <html lang="es">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Ministerio DOULOS - Administración">
        <title>Artículos</title>
        <!-- Favicon -->
        <link rel="icon" href="assets/img/favicon.png" type="image/png">
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
        <!-- Icons -->
        <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
        <link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
        <!-- Page plugins -->
        <link rel="stylesheet" href="assets/vendor/animate.css/animate.min.css">
        <link rel="stylesheet" href="assets/vendor/quill/dist/quill.core.css" type="text/css">
        <link rel="stylesheet" href="assets/vendor/sweetalert2/dist/sweetalert2.min.css">
        <!-- Argon CSS -->
        <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">
    </head>

<body>
<?php require_once ('Sidenav.php')?>
    <!-- Main content -->
    <div class="main-content" id="panel">
    <?php require_once ('Topnav.php')?>
        <div class="header bg-default">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-12">
                            <h6 class="h2 text-white d-inline-block mb-0">Artículos</h6>
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><a href="../index.php"><i class="fas fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="dashboard.php">Main</a></li>
                                    <li class="breadcrumb-item"><a href="articles.php">Articulos</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Artículos - Edición</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
                <div class="row">
                    <div class="col-6">
                        <h3 class="mb-0">Edición de Artículos</h3>
                        <p class="text-sm mb-0">
                            Aquí podrás editar/modificar parámetros de los artículos que publiques tu y los administradores.
                        </p>
                    </div>
                    <div class="col-6 text-right">
                        <?php if ($id>0) {?>
                            <button href="#" id="publicar_art" class="btn btn-lg <?php echo $estaPublicado ? "btn-danger" : "btn-success"; ?> btn-round btn-icon" data-toggle="tooltip" data-original-title="Publicar Artículo">
                                <span class="btn-inner--icon"><i class="<?php echo $estaPublicado ? "fas fa-trash" : "fas fa-check"; ?>"></i></span>
                                <span class="btn-inner--text"><?php echo $estaPublicado ? "Despublicar" : "Publicar"; ?></span>
                            </button>
                        <?php } ?>
                        <button href="#" id="submit-art" class="btn btn-lg btn-outline-primary btn-round btn-icon" data-toggle="tooltip" data-original-title="Guardar Cambios">
                            <span class="btn-inner--icon"><i class="fas fa-save"></i></span>
                            <span class="btn-inner--text">Guardar</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form id="frmarticulo">
                    <input type="hidden" id="Id_Articulo" name="Id_Articulo" value="<?php echo $datos['IdArticulo'];?>"/>
                    <input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $datos['id_usuario'];?>"/>
                    <h6 class="heading-small text-muted mb-4">Información del Articulo</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-title-art">Título del Artículo</label>
                                    <input type="text" name="input-title-art" id="input-title-art" class="form-control" placeholder="Título"
                                           value="<?php echo $datos['Titulo_Art'];?>"/>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-type-art">Tipo</label>
                                    <select class="form-control" id="input-type-art" name="input-type-art">
                                        <option value="P" <?php if ($datos['Tipo_Art']=="P") echo "selected"?>>De Portada</option>
                                        <option value="B" <?php if ($datos['Tipo_Art']=="B") echo "selected"?>>Blog Normal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <label class="form-control-label">Estado</label>
                                    <label class="custom-toggle d-flex align-items-center" for="status-art">
                                        <input id="status-art" name="status-art" type="checkbox" value="<?php echo ($datos['Activo_Art']); ?>" <?php if ($datos['Activo_Art']=="1") echo "checked"?>/>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Si"></span>
                                    </label>
                                </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <div class="form-group">
                                    <label class="form-control-label" for="introtext-art">Texto Introductorio</label>
                                    <div id="div-introtext-art" data-toggle="quill" data-quill-placeholder="Texto a modo de Introducción del Artículo.">
                                        <?php echo $datos['Introtext_Art']?>
                                    </div>
                                    <textarea name="introtext-art" style="display:none" id="introtext-art"></textarea>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="main-img-art">Imagen de Portada</label>
                                    <?php if ($id>0) {?>
                                    <a href="#" class="btn btn-sm btn-outline-info text-right" data-toggle="modal" data-target="#modal-default" >Cambiar</a>
                                    <?php } ?>
                                    <div class="img-thumbnail">
                                        <img class="img-fluid" id="main-img-art" src="<?php echo $datos['UrlImg_Art'] ?>" alt="..." />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="fulltext-art">Cuerpo del Artículo</label>
                                    <div id="div-fulltext-art" data-toggle="quill" data-quill-placeholder="El cuerpo del artículo">
                                        <?php echo $datos['Fulltext_Art']?>
                                    </div>
                                    <textarea name="fulltext-art" style="display:none" id="fulltext-art"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="tags-art">Tags</label>
                                    <input type="text" id="tags-art" name="tags-art" class="form-control" value="<?php echo $datos['Tags_Art']?>" data-toggle="tags" />
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="creation-date-art">Fecha de Creación del Articulo</label>
                                    <input readonly name="creation-date-art" type="text" id="creation-date-art" class="form-control" value="<?php echo $datos['FechaCreacion_Art']?>">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="modif-date-art">Fecha de Modificación del Artículo</label>
                                    <input readonly name="modif-date-art" type="text" id="modif-date-art" class="form-control" value="<?php echo $datos['FechaModificacion_Art']?>">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="public-date-art">Fecha de Publicación del Artículo</label>
                                    <input readonly name="public-date-art" type="text" id="public-date-art" class="form-control" value="<?php echo $datos['FechaPublicacion_Art']?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                 <!--Pop-up/modal para cambiar imagen de portada -->
                <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="modal-title" id="modal-title-default">Cambiar Imagen Principal</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <form id="frm-img-art" name="frm-img-art" class="dropzone"
                                  action="procesos/img-upload-art.php"
                                  method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                                <p class="text-danger"><u>*La imagen NO debe superar los 3MB.</u></p>
                                <p>La imagen que cargará en este campo se guardará automáticamente. Luego de subirla pulse al boton "Salir".</p>
                                <div id="dropzone-form" class="dropzone dropzone-single mb-3"
                                     data-toggle="dropzone"
                                     data-dropzone-url="procesos/img-upload-art.php"
                                     data-dropzone-id="id-img-art">
                                    <div class="fallback">
                                        <div class="custom-file">
                                            <input type="file" id="fichero" name="fichero" class="custom-file-input"  />
                                            <input type="hidden" id="id-img-art" name="id-img-art" value="<?php echo $id?>">
                                            <label class="custom-file-label" for="fichero">Escoger archivo</label>
                                        </div>
                                    </div>
                                    <div class="dz-preview dz-preview-single">
                                        <div class="dz-preview-cover">
                                            <img class="dz-preview-img" src="<?php echo $datos['UrlImg_Art']?>" alt="..." data-dz-thumbnail>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="img-upload" type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Salir</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/js-cookie/js.cookie.js"></script>
    <script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
    <script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>
    <script src="assets/vendor/quill/dist/quill.min.js"></script>
    <script src="assets/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="assets/vendor/dropzone/dist/min/dropzone.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <!-- Argon JS -->
    <script src="assets/js/argon.js?v=1.2.0"></script>

    <script>
        $('#submit-art').click(function() {
            let quills=$('[data-toggle="quill"]');
            let q1 = new Quill(quills[0]); // igual a introtext-art
            let q2 = new Quill(quills[1]); // igual a fulltext-art
            $("#introtext-art").val(q1.root.innerHTML);
            $("#fulltext-art").val(q2.root.innerHTML);
            document.getElementById("status-art").value= document.getElementById("status-art").checked ? '1' : '0';

            datos = $('#frmarticulo').serialize();
            $.ajax({
                type: "POST",
                data: datos,
                url: "procesos/savearticle.php",
                success: function (r) {
                    console.log(r);
                    let obj;
                    obj = JSON.parse(r);
                    if (obj.result) {
                        Swal.fire({
                            title: obj.title,
                            text: obj.message,
                            icon: "success",
                            showCancelButton: false,
                            confirmButtonText: 'OK'
                            })
                            .then((value) => {
                                window.location="articles.php";
                            });
                    }
                    else Swal.fire(obj.title, obj.message, "warning");

                }
            });
        });//GUARDAR articulo

        $('#publicar_art').click(function() {
            let id =document.getElementById('Id_Articulo').value;
            let fecha=document.getElementById('public-date-art').value;
            let modo=-1;
            /*if(fecha=='')
                modo=1;
            else
                modo=0;*/
            modo = fecha=='' ? 1 : 0;
            $.ajax({
                type:"POST",
                data:JSON.stringify({ 'IdArticulo': id, 'modo': modo }),
                dataType: "json",
                url:"procesos/publicar_art.php",
                success:function(r){
                    console.log(r);
                    if(r.result){
                        Swal.fire({
                            title: r.title,
                            text: r.message,
                            icon:'success',
                            confirmButtonText: 'OK'
                        })
                            .then((value) => {
                                window.location="articles.php";
                            });
                    }else{
                        Swal.fire('Error en Base de datos', r.message, "error");
                    }
                }
            });
        });//PUBLICAR articulo

        $('#img-upload').click(function () {
            location.reload();
        })
    </script>
</body>
</html>


