<?php
//Leer session activa
session_start();

//Destruir las cookies generadas de sesion
session_destroy();

//redirigir a archivo index.php
header("location:../index.php");
?>