<?php include ('procesos/security-login.php');
require_once (dirname(__FILE__) . "/../clases/conexion.php");
require_once (dirname(__FILE__) . "/../clases/archivos.php");
require_once (dirname(__FILE__) . "/../clases/usuarios.php");
$obj= new archivos();
//$_SESSION['iduser'] esto tiene IdUser
$datos = $obj->TablaDatosArchivo();
//var_dump($datos);
?>
<!DOCTYPE html>

<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Ministerio DOULOS - Administración">
    <title>Archivos</title>
    <!-- Favicon -->
    <link rel="icon" href="assets/img/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <!-- Page plugins -->
    <link rel="stylesheet" href="assets/vendor/sweetalert2/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="assets/vendor/animate.css/animate.min.css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">
    <script src="assets/js/validar-archivos.js"></script>
</head>

<body>
<?php require_once ('Sidenav.php')?>
<!-- Main content -->
<div class="main-content" id="panel">
    <?php require_once ('Topnav.php')?>
    <!-- Header -->
    <div class="header bg-default pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Archivos</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="../index.php"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="dashboard.php">Main</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Archivos</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <!-- Table -->
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <h3 class="mb-0">Tabla de datos de Archivos</h3>
                        <p class="text-sm mb-0">
                            Aquí podrás encontrar los archivos que hayas publicado en la sección Archivos,
                            y podrás gestionarlos de manera cómoda <strong>aquí</strong>.</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th>Usuario</th>
                                    <th>Título</th>
                                    <th>Descripción</th>
                                    <th>Tamaño</th>
                                    <th>Tipo</th>
                                    <th>Fecha</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach ($datos as $row) {
                                    //var_dump($row);
                                    ?>
                                    <tr>
                                        <td><?php echo $row['author']?></td>
                                        <td><?php echo $row['titulo']?></td>
                                        <td><?php echo $row['descripcion']?></td>
                                        <td><?php echo $row['size']?></td>
                                        <td><?php echo $row['tipo']?></td>
                                        <td><?php echo $row['fecha_creacion']?></td>
                                        <td class="table-actions">
                                            <button name="btn-modal" class="btn btn-sm btn-primary" type="button" data-toggle="modal" data-idarchive="<?php echo $row['id'] ?>"> <!--data-target="#modal-default"-->
                                                <span class="btn-inner--icon" data-toggle="tooltip" data-original-title="Editar"><i class="fas fa-edit"></i></span>
                                            </button>
                                            <button name="delete-archive" class="btn btn-sm btn-outline-danger" type="button" data-idarchive-delete="<?php echo $row['id'] ?>">
                                                <span class="btn-inner--icon" data-toggle="tooltip" data-original-title="Eliminar"><i class="fas fa-trash"></i></span>
                                            </button>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Usuario</th>
                                    <th>Título</th>
                                    <th>Descripción</th>
                                    <th>Tamaño</th>
                                    <th>Tipo</th>
                                    <th>Fecha</th>
                                    <th>Acción</th>
                                </tr>
                                </tfoot>
                            </table>

                            <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                                <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6 class="modal-title">Edición del Archivo</h6>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="edit-archive">
                                                <input type="hidden" id="id_modal">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="titulo">Título</label>
                                                    <input type="text" class="form-control" name="titulo"
                                                           id="titulo" placeholder="Titulo del archivo">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-control-label" for="descripcion">Descripción</label>
                                                    <textarea type="text" name="descripcion" class="form-control" id="descripcion"
                                                              rows="3" placeholder="Descripcion del archivo"></textarea>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label" for="type">Tipo</label>
                                                            <input type="text" class="form-control" name="type"
                                                                   id="type" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label" for="size">Tamaño</label>
                                                            <input type="text" class="form-control" name="size"
                                                                   id="size" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label" for="fecha-creacion">Fecha de Creación</label>
                                                            <input type="text" class="form-control" name="fecha-creacion"
                                                                   id="fecha-creacion" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="img-thumbnail">
                                                    <img class="img-fluid" id="preview" src="" alt="..." />
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="btn_savearchive" class="btn btn-outline-success btn-icon btn-lg btn-block">
                                                <span class="btn-inner--icon">
                                                    <i class="ni ni-cloud-upload-96"></i>
                                                </span>
                                                <span class="btn-inner--text">Guardar</span>
                                            </button>
                                            <button type="button" class="btn btn-outline-danger  ml-auto" data-dismiss="modal">Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Browser de Archivos - Dropzone -->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <div class="row">
                            <div class="col-10 text-left">
                                <h2 class="mb-0">Subir Archivo</h2>
                            </div>
                            <div class="col-2 text-right">
                                <button class="btn btn-icon btn-success" type="button" onclick="location.reload()">
                                    <span class="btn-inner--icon"><i class="fas fa-refresh"></i></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        <p class="text-warning">
                            Solo se puede subir un archivo a la vez, que no sobrepase los 20 MB.<br>
                            *Solo se permiten archivos del tipo img/pdf (.pdf, .jpeg, .png y derivados)</p>
                        <form action="procesos/subir-archivos.php"
                              id="dropzone-form2"
                              method="post" enctype="multipart/form-data">
                            <div id="dropzone-form" class="dropzone dropzone-single mb-3"
                                 data-toggle="dropzone"
                                 data-dropzone-title="titulo"
                                 data-dropzone-description="descripcion"
                                 data-dropzone-url="procesos/subir-archivos.php">
                                <div class="fallback">
                                    <div class="custom-file">
                                        <input type="file" id="fichero" name="fichero" class="custom-file-input" >
                                        <label class="custom-file-label" for="projectCoverUploads">Escoger archivo</label>
                                    </div>
                                </div>
                                <div class="dz-preview dz-preview-single">
                                    <div class="dz-preview-cover">
                                        <img class="dz-preview-img" src="../img/blog/default.jpg" alt="..." data-dz-thumbnail>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Argon Scripts -->
<!-- Core -->
<script src="assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/js-cookie/js.cookie.js"></script>
<script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
<script src="assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="assets/vendor/dropzone/dist/min/dropzone.min.js"></script>
<script src="assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>
<!-- Argon JS -->
<script src="assets/js/argon.js?v=1.2.0"></script>
<script>
    //Traer info del archivo
    $("button[name='btn-modal']").click(function() {
        //$('.btn-modal').click(function () {
        var id = this.getAttribute('data-idarchive');
        $.ajax({
            type:"POST",
            data:JSON.stringify({ 'id': id }),
            dataType: "json",
            url:"procesos/trae-info-archivo.php",
            success:function(r){
                console.log(r);
                if(r.result){
                    document.getElementById('id_modal').value=r.datos.id;
                    document.getElementById('titulo').value=r.datos.titulo;
                    document.getElementById('descripcion').value=r.datos.descripcion;
                    document.getElementById('type').value=r.datos.tipo;
                    document.getElementById('size').value=r.datos.size;
                    document.getElementById('fecha-creacion').value=r.datos.fecha_creacion;
                    document.getElementById('preview').src=r.datos.ruta;
                    $("#modal-default").modal({ backdrop: "static" });
                }else{
                    Swal.fire('Error en Base de datos', 'No se pudo actualizar', "error");
                }
            }
        });

    });
    //Eliminar archivo
    $("button[name='delete-archive']").click(function() {

        let id = this.getAttribute('data-idarchive-delete');

        swal({
            title: 'Estás Seguro?',
            text: "No podrás revertir esta acción",
            type: 'warning',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-danger',
            confirmButtonText: 'Si, Eliminar',
            cancelButtonClass: 'btn btn-secondary',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type:"POST",
                    data:JSON.stringify({ 'id': id }),
                    dataType: "json",
                    url: 'procesos/deletearchive.php',
                    success:function (r) {
                        console.log(r);
                        if (r.result){
                            swal({
                                title: r.title,
                                text: r.message,
                                type: 'success',
                                buttonsStyling: false,
                                confirmButtonClass: 'btn btn-primary'
                            })
                                .then((value) => {
                                    location.reload();
                                });
                        }
                        else swal(r.title, r.message, "error");
                    }

                });

            }
        })
    });
    //Guardar cambios en archivos
    $('#btn_savearchive').click(function(){
        datos=JSON.stringify({ 'id': document.getElementById("id_modal").value,
            'titulo': document.getElementById("titulo").value,
            'descripcion': document.getElementById("descripcion").value,
        });

        if (validar() === false)
            return false;
        else {
            $.ajax({
                type:"POST",
                data:datos,
                url:"procesos/savearchive.php",
                success:function(r){
                    r=JSON.parse(r);
                    console.log(r);

                    if(r.result){
                        Swal({
                            title: r.title,
                            text: r.message,
                            type:'success',
                            confirmButtonText: 'OK'
                        })
                            .then((value) => {
                                location.reload();
                            });
                        $("#modal-default").modal('hide');
                    }
                    else
                        Swal.fire(r.title,r.message,'error');
                }
            });
        }
    });
</script>

</body>
</html>

