    <!-- =========================================================
* Argon Dashboard v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)



=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 -->
<?php include ('procesos/security-login.php');

require_once (dirname(__FILE__) . "/../clases/conexion.php");
require_once (dirname(__FILE__) . "/../clases/usuarios.php");
$obj= new usuarios();
//$_SESSION['iduser'] esto tiene IdUser
$user = $obj->obtenDatosUsuario($_SESSION['iduser']);
//var_dump($user);
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Ministerio DOULOS - Administración">
  <title>Tu perfil</title>
  <!-- Favicon -->
  <link rel="icon" href="assets/img/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <link rel="stylesheet" href="assets/vendor/animate.css/animate.min.css">
  <link rel="stylesheet" href="assets/vendor/sweetalert2/dist/sweetalert2.min.css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">

  <script src="assets/js/validar-profile.js"></script>
</head>

<body>
  <?php require_once ('Sidenav.php')?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <?php require_once ('Topnav.php')?>
    <!-- Header -->
    <!-- Header -->
    <div class="header pb-6 d-flex align-items-center" style="min-height: 500px; background-image: url(assets/img/profile-cov.jpg); background-size: cover; background-position: center top;">
      <!-- Mask -->
      <span class="mask bg-gradient-default opacity-8"></span>
      <!-- Header container -->
      <div class="container-fluid d-flex align-items-center">
        <div class="row">
          <div class="col-lg-7 col-md-10">
            <h1 class="display-2 text-white">Hola <?php echo $user['nombre'];?>!</h1>
            <p class="text-white mt-0 mb-5">Esta es tu página de perfil. Aquí puedes editar la información de tu perfil y de contacto. </p>
            <a href="#edit_profile" class="btn btn-sm btn-outline-info">Editar perfil</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-12 order-xl-1">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0" id="edit_profile">Edita tu perfil </h3>
                </div>
                <div class="col-4 text-right">
                  <a href="#" class="btn btn-primary btn-round btn-icon" id="hrefsubmit">
                      <span class="btn-inner--icon"><i class="fas fa-save"></i></span>
                      <span class="btn-inner--text">Guardar</span>
                  </a>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form id="frmprofile">
                  <input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $user['id_usuario'];?>">
                <h6 class="heading-small text-muted mb-4">Información del usuario</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Nombre de Usuario</label>
                        <input type="text" id="input_username" class="form-control" placeholder="Usuario"
                               value="<?php echo $user['username'];?>"
                               readonly
                        >
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Dirección de Correo Electrónico</label>
                        <input name="input-email" type="email" id="input-email" class="form-control" placeholder="unknown@example.com"
                            value="<?php echo $user['email'];?>" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-first-name">Tu nombre</label>
                        <input name="input-first-name" type="text" id="input_first_name" class="form-control" placeholder="Nombre"
                               value="<?php echo $user['nombre'];?>" required>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-last-name">Tu apellido</label>
                        <input name="input-last-name" type="text" id="input_last_name" class="form-control" placeholder="Apellido"
                               value="<?php echo $user['apellido'];?>" required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-6">
                          <div class="form-group">
                              <label class="form-control-label" for="input-password">Contraseña</label>
                              <input name="input-password" type="password" id="input_password" class="form-control"
                                     value="">
                          </div>
                      </div>
                      <div class="col-lg-6">
                          <div class="form-group">
                              <label class="form-control-label" for="repeat-password">Repite tu contraseña</label>
                              <input name="repeat-password" type="password" id="repeat_password" class="form-control" value="">
                          </div>
                      </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-muted mb-4">Información de contacto</h6>
                <div class="pl-lg-4">
                  <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="form-control-label" for="input-address">Dirección</label>
                            <input name="input-address" id="input_address" class="form-control" placeholder="Tu dirección"
                                   value="<?php echo $user['direccion'];?>" type="text">
                          </div>
                        </div>
                      </div>
                  <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="form-control-label" for="input-city">Ciudad</label>
                            <input name="input-city" type="text" id="input_city" class="form-control" placeholder="Ciudad donde vives"
                                   value="<?php echo $user['ciudad'];?>">
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="form-control-label" for="input-number">Número Telefónico</label>
                            <input name="input-number" type="tel" pattern="[0-9]{9}" maxlength="12" id="input_number" class="form-control"
                                   placeholder="Tu número de celular" value="<?php echo $user['telefono'];?>">
                          </div>
                        </div>
                      </div>
                </div>
                <hr class="my-4" />
                <!-- Description -->
                <h6 class="heading-small text-muted mb-4">Acerca de ti</h6>
                <div class="pl-lg-4">
                  <div class="form-group">
                    <label class="form-control-label" for="about">Acerca de ti</label>
                    <textarea name="about" rows="4" class="form-control" id="about"
                               placeholder="Solo unas cuantas palabras..."><?php echo $user['about'];?></textarea>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <!-- Argon JS -->
  <script src="assets/js/argon.js?v=1.2.0"></script>

  <script src="../js/ini.js"></script>

<script>
    $('#hrefsubmit').click(function(){

        //validaciones
        if (validar() === false)
            return false;
        else {

            datos=$('#frmprofile').serialize();
            $.ajax({
                type:"POST",
                data:datos,
                url:"procesos/saveprofile.php",
                success:function(r){
                    console.log(r);
                    if(r == 1){
                        swal.fire({
                            title: "Hecho!",
                            text: "Los cambios se han guardado satisfactoriamente!",
                            icon: "success",
                            buttons: true,
                            dangerMode: true,
                        })
                            .then((value) => {
                                window.location="profile.php";
                            });

                     }else{
                        swal('Error en Base de datos', 'No se pudo actualizar');
                    }
                }
            });
        }
    });
</script>
</body>

</html>