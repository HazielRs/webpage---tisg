
<!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header d-flex align-items-center">
        <a class="navbar-brand" href="dashboard.php">
          <img src="assets/img/BLogo2.png" class="navbar-brand-img" alt="...">
        </a>
        <div class="ml-auto">
          <!-- Sidenav toggler -->
          <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
              <li class="nav-item">
                  <a class="nav-link" href="../index.php">
                      <i class="ni ni-app text-cyan"></i>
                      <span class="nav-link-text">DOULOS Ministery</span>
                  </a>
              </li>
            <li class="nav-item">
              <a class="nav-link" href="dashboard.php">
                <i class="ni ni-shop text-primary"></i>
                <span class="nav-link-text">Administración</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#navbar-profile" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-profile">
                <i class="ni ni-single-02 text-yellow"></i>
                <span class="nav-link-text">Perfil</span>
              </a>
              <div class="collapse" id="navbar-profile">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="profile.php" class="nav-link">Tu Perfil</a>
                  </li>
                  <li class="nav-item disabled">
                    <a href="messages.php" class="nav-link disabled">Mensajes</a>
                  </li>
                </ul>
              </div>
            </li>
            <li class="nav-item disabled">
              <a class="nav-link disabled" href="events.php">
                <i class="ni ni-istanbul text-gray"></i> <!--blue-->
                <span class="nav-link-text">Eventos</span>
              </a>
            </li>
            <li class="nav-item disabled">
              <a class="nav-link disabled" href="projects.php">
                <i class="ni ni-book-bookmark text-gray"></i> <!--danger-->
                <span class="nav-link-text">Proyectos</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="articles.php">
                <i class="ni ni-books text-default"></i>
                <span class="nav-link-text">Artículos</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="users.php">
                <i class="ni ni-circle-08 text-green"></i>
                <span class="nav-link-text">Usuarios</span>
              </a>
            </li>
              <li class="nav-item">
                  <a class="nav-link" href="archives.php">
                      <i class="ni ni-archive-2 text-primary"></i>
                      <span class="nav-link-text">Archivos</span>
                  </a>
              </li>
            <li class="nav-item">
              <a class="nav-link" href="calendar.php">
                <i class="ni ni-calendar-grid-58 text-red"></i>
                <span class="nav-link-text">Calendario</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>