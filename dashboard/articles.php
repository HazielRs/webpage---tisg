<?php include ('procesos/security-login.php');
require_once (dirname(__FILE__) . "/../clases/conexion.php");
require_once (dirname(__FILE__) . "/../clases/articulos.php");
$obj=new articulos();
$datos=$obj->obtenAllDatos();
//var_dump($datos);
?>
<!DOCTYPE html>

<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Ministerio DOULOS - Administración">
    <title>Artículos</title>
    <!-- Favicon -->
    <link rel="icon" href="assets/img/favicon.png" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <!-- Page plugins -->
    <link rel="stylesheet" href="assets/vendor/sweetalert2/dist/sweetalert2.min.css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body>
    <?php require_once ('Sidenav.php')?>
    <!-- Main content -->
    <div class="main-content" id="panel">
        <?php require_once ('Topnav.php')?>
        <!-- Header -->
        <div class="header bg-default pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <h6 class="h2 text-white d-inline-block mb-0">Artículos</h6>
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><a href="../index.php"><i class="fas fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="dashboard.php">Main</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Artículos</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-6 col-5 text-right">
                            <a href="articles-edit.php" class="btn btn-neutral">Crear nuevo artículo</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
            <!-- Table -->
            <div class="row">
                <div class="col">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">
                            <h3 class="mb-0">Tabla de datos de Artículos</h3>
                            <p class="text-sm mb-0">
                            Aquí podrás encontrar los articulos que hayas publicado y podrás gestionarlos de manera cómoda.</p>
                        </div>
                        <div class="table-responsive py-4">
                            <table class="table table-flush" id="datatable-basic">
                                <thead class="thead-light">
                                <tr>
                                    <th>Titulo</th>
                                    <th>Autor</th>
                                    <th>Fecha de Creación</th>
                                    <th>Nro. de Visitas</th>
                                    <th>Activo</th>
                                    <th>Acción</th>

                                </tr>
                                </thead
                                <tbody>
                                <?php foreach ($datos as $row) {
                                    //var_dump($row);
                                    ?>
                                    <tr>
                                        <td><?php echo $row['Titulo_Art']?></td>
                                        <td><?php echo $row['author']?></td>
                                        <td><?php echo $row['FechaCreacion_Art']?></td>
                                        <td><?php echo $row['Visitas_Art']?></td>
                                        <!--<td>?></td>-->
                                        <td>
                                              <span class="badge badge-dot mr-4">
                                                <i class="<?php if($row['Activo_Art']=="1") echo "bg-success"; else echo "bg-warning"?>"></i>
                                              <span class="status"><?php if($row['Activo_Art']=="1") echo "Habilitado"; else echo "Oculto"?></span>
                                        </td>
                                        <td class="table-actions justify-content-center">
                                            <a href="articles-edit.php?id=<?php echo ($row['IdArticulo'])?>" class="table-action text-center" data-toggle="tooltip" data-original-title="Editar">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <button name="delete-art" class="btn btn-sm btn-outline-danger" type="button" data-idart-delete="<?php echo $row['IdArticulo'] ?>">
                                                <span class="btn-inner--icon" data-toggle="tooltip" data-original-title="Eliminar"><i class="fas fa-trash"></i></span>
                                            </button>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Titulo</th>
                                    <th>Autor</th>
                                    <th>Fecha de Creación</th>
                                    <th>Nro. de Visitas</th>
                                    <th>Activo</th>
                                    <th>Acción</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- Argon Scripts -->
      <!-- Core -->
      <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
      <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
      <script src="assets/vendor/js-cookie/js.cookie.js"></script>
      <script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
      <script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
    <script src="assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <!-- Argon JS -->
  <script src="assets/js/argon.js?v=1.2.0"></script>
    <script>
        $("button[name='delete-art']").click(function() {

            let id = this.getAttribute('data-idart-delete');

            swal({
                title: 'Estás Seguro?',
                text: "No podrás revertir esta acción",
                type: 'warning',
                showCancelButton: true,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-danger',
                confirmButtonText: 'Si, Eliminar',
                cancelButtonClass: 'btn btn-secondary',
                cancelButtonText: 'Cancelar',
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type:"POST",
                        data:JSON.stringify({ 'IdArticulo': id }),
                        dataType: "json",
                        url: 'procesos/deletearticle.php',
                        success:function (r) {
                            console.log(r);
                            if (r.result){
                                swal({
                                title: r.title,
                                text: r.message,
                                type: 'success',
                                buttonsStyling: false,
                                confirmButtonClass: 'btn btn-primary'
                            })
                                .then((value) => {
                                    location.reload();
                                });
                                }
                            else swal(r.title, r.message, "error");
                        }

                    });

                }
            })
        });
    </script>
</body>