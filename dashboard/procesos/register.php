<?php 
	session_start();
	require_once (dirname(__FILE__) . "/../../clases/conexion.php");
	require_once (dirname(__FILE__) . "/../../clases/usuarios.php");
    require_once (dirname(__FILE__) . "/../../clases/response.php");
    require_once (dirname(__FILE__) . "/../../clases/helper.php");

	//    $ret = array('result'=> false, 'title'=> 'Error!', 'message'=>'Error en parámetros! :(');
// var_dump($_POST);
    $ret = new ResponseModel();

    if(!isset($_POST['newUsername']) || $_POST['newUsername']==''){
        $ret->message = 'El campo Nombre de Usuario es obligatorio';
        echo json_encode($ret);
        return;
    }
    else if(!isset($_POST['newEmail']) || $_POST['newEmail']==''){
        $ret->message = 'El campo Email es obligatorio';
        echo json_encode($ret);
        return;
    }
    else if(!isset($_POST['newPassword']) || $_POST['newPassword']==''){
        $ret->message = 'El campo Contraseña es obligatorio';
        echo json_encode($ret);
        return;
    }
    else if(!isset($_POST['newName']) || $_POST['newName']==''){
        $ret->message = 'El campo Nombre es obligatorio';
        echo json_encode($ret);
        return;
    }
    else if(!isset($_POST['newSurname']) || $_POST['newSurname']==''){
        $ret->message = 'El campo Apellidos es obligatorio';
        echo json_encode($ret);
        return;
    }
    else if(!isset($_POST['input-city']) || $_POST['input-city']==''){
        $ret->message = 'El campo Ciudad es obligatorio';
        echo json_encode($ret);
        return;
    }
    else if(!isset($_POST['input-number']) || $_POST['input-number']==''){
        $ret->message = 'El campo Número Telefónico es obligatorio';
        echo json_encode($ret);
        return;
    }
    else if(!isset($_POST['input-address']) || $_POST['input-address']==''){
        $ret->message = 'El campo Dirección es obligatorio';
        echo json_encode($ret);
        return;
    }


    if(strlen(trim($_POST['newUsername']))>20){
        $ret->message = 'El campo Nombre de Usuario es muy largo!';
        echo json_encode($ret);
        return;
    }
    if(strlen(trim($_POST['newEmail']))>30){
    $ret->message = 'El campo Email es muy largo!';
    echo json_encode($ret);
    return;
}
    if(strlen(trim($_POST['newPassword']))>25){
    $ret->message = 'El campo Contraseña es muy largo!';
    echo json_encode($ret);
    return;
}
    if(strlen(trim($_POST['newName']))>20){
        $ret->message = 'El campo Nombre es muy largo!';
        echo json_encode($ret);
        return;
    }
    if(strlen(trim($_POST['newSurname']))>40){
        $ret->message = 'El campo Apellidos es muy largo!';
        echo json_encode($ret);
        return;
    }
    if(strlen(trim($_POST['input-city']))>20){
    $ret->message = 'El campo ciudad es muy largo!';
    echo json_encode($ret);
    return;
}
    if(strlen(trim($_POST['input-number']))>9){
    $ret->message = 'El campo Numero telefonico es muy largo!';
    echo json_encode($ret);
    return;
}
    if(strlen(trim($_POST['input-address']))>40){
        $ret->message = 'El campo Dirección es muy largo!';
        echo json_encode($ret);
        return;
    }

    if (!preg_match('/[1-9]{2}[0-9]{5}$/', $_POST['input-number'])){
        $ret->message = 'El campo teléfono no es válido!';
    echo json_encode($ret);
    return;
    }

    $val=new helper();
    $vl=$val->is_valid_email(trim($_POST['newEmail']));
    //var_dump($vl);
    if ($vl === false){
        $ret->message = 'El campo Email no es válido!';
        echo json_encode($ret);
        return;
    }

    $obj= new usuarios();

    //var_dump(json_encode($ret));
    //definir array a devolver
    
    try{

        $datos=array(
            "username" =>trim($_POST['newUsername']),
            "nombre" =>trim($_POST['newName']),
            "apellido" =>trim($_POST['newSurname']),
            "email" => trim($_POST['newEmail']),
            "password" => trim($_POST['newPassword']),
            "isadministrador" => 0,
            "direccion" => trim($_POST['input-address']),
            "ciudad" => trim($_POST['input-city']),
            "telefono" => trim($_POST['input-number']),
        );
        //var_dump($datos);
        
        //Buscar si existe Email
        $res=$obj->ExisteEmail($_POST['newEmail']);
        if ($res>0)
        {
            $ret->message='El correo: '. $_POST['newEmail'] . ', ya existe en la base de datos' ;
            echo json_encode($ret);
            return;
        }
         //Buscar si existe UserName
        $res=$obj->ExisteUserName($_POST['newUsername']);
        if ($res>0)
        {
            $ret->message='Este Username: '. $_POST['newUsername'] . ' ya existe en la base de datos';
            echo json_encode($ret);
            return;
        }

        //si no hay errores, recien grabar
    	$res=$obj->registroUsuario($datos);
    	if ($res>0) //si Id es Mayor a cero, se grabo
        {
            $ret->result=true;
            $ret->title='Registro Exitoso';
            $ret->message="Haga clic en el botón para regresar a la pagina de iniciar sesión...";
        }
    	else { //Si hubo un error interno
            $ret->title='Error desconocido de la base de datos';
        }
        //var_dump($ret);
        //$ret=array("res"=> true, "msg"=> 'Registro Exitoso');
    
    } catch (Exception $e) {
        $ret->title='ERROR FATAL ->'. $e->getMessage();
    }

    echo json_encode($ret);
 ?>