<?php

require_once (dirname(__FILE__) . "/security-login.php");
require_once (dirname(__FILE__) . "/../../clases/conexion.php");
require_once (dirname(__FILE__) . "/../../clases/calendar.php");

$accion = (isset($_GET['accion'])) ? ($_GET['accion']) : 'leer';
$obj= new calendar();
switch ($accion){
    case 'agregar':
        //var_dump($_POST);
        $allDay=1;
        if(isset($_POST['start']))
            $start =date('Y-m-d',strtotime($_POST['start']));
            //$start =getdate(strtotime($_POST['start']));
        //var_dump($start);
        if(isset($_POST['end']))
            $end=date('Y-m-d', strtotime($_POST['end']));
            //$end =getDate(strtotime($_POST['end']));
        if(isset($_POST['allDay']))
            $allDay = $_POST['allDay']=='true' ? 1 : 0;
        $datos=array(
            "title" => $_POST['title'],
            "description" => $_POST['description'],
            "className" => $_POST['className'],
            "start" => $start,
            "end" => $end,
            "allDay" => $allDay);
        $ret = $obj->registro($datos);
        echo json_encode($ret>0);
        break;
    case 'modificar':
       //var_dump($_POST);
//        var_dump($_GET);
        $allDay=1;
        $end = '';
        if(isset($_POST['start']))
            $start = date('Y-m-d',strtotime($_POST['start']));
        if(isset($_POST['end']))
            if($_POST['end']!='')
                $end =date('Y-m-d',strtotime($_POST['end']));
        if(isset($_POST['allDay']))
            $allDay = $_POST['allDay']=='true' ? 1 : 0;

        if($end=='')
            $end=$start;
        $datos=array(
            "id" => $_POST['id'],
            "title" => $_POST['title'],
            "description" => $_POST['description'],
            "className" => $_POST['className'],
            "start" => $start,
            "end" => $end,
            "allDay" => $allDay);
        $ret = $obj->modificar($datos);
        echo json_encode($ret>0);
        break;
    case 'eliminar':

            if (isset($_POST['id']))
                $id=$_POST['id'];
        $ret = $obj->eliminar($id);
        echo json_encode($ret>0);
        break;
    default:
        //se tiene que recibir un parametro de fecha
        //var_dump($_GET);
        $date = date('Y-m-d');
        $start = strtotime ( '+1 month' , strtotime ( $date ) ) ; //mes siguiente del $date
        $start  = date ( 'Y-m-j' , $start );

        $end  = strtotime ( '-1 month' , strtotime ( $date ) ) ;
        $end = date (  'Y-m-j' , $end );   //mes anterior del $date

        if(isset($_GET['start']))
           $start = date('Y-m-d',strtotime($_GET['start']));
        if(isset($_GET['end']))
            $end =date('Y-m-d',strtotime($_GET['end']));
        $registros=$obj->obtenAllDatosForMes($start, $end);
        //var_dump($registros);
        echo json_encode($registros);
        break;
}
