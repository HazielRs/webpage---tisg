<?php
session_start();
require_once (dirname(__FILE__) . "/../../clases/conexion.php");
require_once (dirname(__FILE__) . "/../../clases/archivos.php");
require_once (dirname(__FILE__) . "/../../clases/response.php");
//var_dump($_POST);
$datos=json_decode(file_get_contents('php://input'), true);
//var_dump($datos);
$res= new ResponseModel();
$obj= new archivos();
$registro=$obj->obtenDatosArchivo(($datos['id']));
//var_dump($registro);
//var_dump($datos);
if ($registro==null || $registro==false){
    $res->message="Datos Inválidos :(";
   echo json_encode($res);
   return;
}
$registro['titulo']=$datos['titulo'];
$registro['descripcion']=$datos['descripcion'];
if ($datos['titulo']==''){
    $res->message="No se asignó titulo alguno al archivo!";
echo json_encode($res);
return;
}
if ($datos['descripcion']==''){
    $res->message="No se asignó ninguna descripción al archivo!";
    echo json_encode($res);
    return;
}
$val=intval( $obj->ExisteTituloArchivo($registro['titulo']) );
if ($val>0) {
    $res->message='Este Título: '. $registro['titulo'] . ' ya existe en la base de datos';
    echo json_encode($ret);
    return;
}
$ret=$obj->actualizaArchivo($registro);

if ($ret>0){
    $res->result=true;
    $res->title='Éxito!';
    $res->message='Archivo Actualizado satisfactoriamente!';

}
else {
    $res->title='Error!';
    $res->message='Error en Base de Datos, no se actualizaron los datos.';
}
echo json_encode($res);

