<?php
session_start();
require_once (dirname(__FILE__) . "/../../clases/conexion.php");
require_once (dirname(__FILE__) . "/../../clases/archivos.php");
//var_dump($_POST);
$c=new conectar();
$datos=json_decode(file_get_contents('php://input'), true);
$obj= new archivos();
$registro=$obj->obtenDatosArchivo($datos['id']);
//var_dump($registro);
if (substr($registro['ruta'], 0,5)!="http:"){
    $registro['ruta']=$c->urlbase . $registro['ruta'];
}
//var_dump($registro['ruta']);
$ret=array(
    'result'=> ($registro==null || $registro==false) ? false:true,
    'datos'=> $registro,
);

echo json_encode($ret);
?>
