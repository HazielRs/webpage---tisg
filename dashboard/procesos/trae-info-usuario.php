<?php
session_start();
require_once (dirname(__FILE__) . "/../../clases/conexion.php");
require_once (dirname(__FILE__) . "/../../clases/usuarios.php");
//var_dump($_POST);
$datos=json_decode(file_get_contents('php://input'), true);
$obj= new usuarios();
$registro=$obj->obtenDatosUsuario($datos['iduser']);
//var_dump($registro);
$registro['password']=''; //bug Seguridad no mostrar Clave a nivel de vista de usuario
$ret=array(
    'result'=> ($registro==null || $registro==false) ? false:true,
    'datos'=> $registro,
);

echo json_encode($ret);
?>
