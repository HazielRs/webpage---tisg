<?php
session_start();
require_once (dirname(__FILE__) . "/../../clases/conexion.php");
require_once (dirname(__FILE__) ."/../../clases/usuarios.php");
//var_dump($_POST);
$datos=json_decode(file_get_contents('php://input'), true);
//var_dump($datos);
$ret=array(
    'result'=> false,
    'msg'=> 'Datos inválidos',
);
$obj= new usuarios();
$registro=$obj->obtenDatosUsuario($datos['id_usuario']);
//var_dump($registro);
//var_dump($datos);
if ($registro==null || $registro==false){
   echo json_encode($ret);
   return;
}
$registro['nombre']=$datos['nombre'];
$registro['apellido']=$datos['apellido'];
if ($datos['isadministrador']=="A")
    $registro['isadministrador']="1";
else
    $registro['isadministrador']="0";

if ($datos['activo']==true)
    $registro['activo']="1";
else
    $registro['activo']="0";
$registro['telefono']=$datos['telefono'];
if ($datos['password']!=="")
    $registro['password']=sha1($datos['password']);
$res=$obj->actualizaUsuario($registro);

if ($res>0){
    $ret['result']=true;
    $ret['title']='Éxito!';
    $ret['message']='Datos Actualizados satisfactoriamente!';

}
else {
    $ret['title']='Error!';
    $ret['message']='Error en Base de Datos, no se actualizaron los datos.';
}
echo json_encode($ret);

