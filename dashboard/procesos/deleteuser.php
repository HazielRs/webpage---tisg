<?php
session_start();
require_once (dirname(__FILE__) . "/../../clases/conexion.php");
require_once (dirname(__FILE__) . "/../../clases/usuarios.php");
require_once (dirname(__FILE__) . "/../../clases/response.php");

$dato=json_decode(file_get_contents('php://input'), true);
$obj= new usuarios();
$res = new ResponseModel();
$id_delete = $obj->eliminaUsuario($dato['id_usuario']);
if ($id_delete>0){
    $res->result = true;
    $res->title = 'Éxito!';
    $res->message = 'Se eliminó al usuario satisfactoriamente!';
}
else{

    $res->title = 'Error!';
    $res->message = 'Error al eliminar el usuario seleccionado!';
}
echo json_encode($res);