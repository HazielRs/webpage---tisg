<?php
/*//recibe solo un $id*/
session_start();
require_once (dirname(__FILE__) . "/../../clases/conexion.php");
require_once (dirname(__FILE__) . "/../../clases/articulos.php");
require_once (dirname(__FILE__) . "/../../clases/response.php");

$datos=json_decode(file_get_contents('php://input'), true);
$id= -1;
$modo =-1;  //1=publicar, otro=Despublicar
try {
    if (isset($datos['IdArticulo']))
        $id = intval($datos['IdArticulo']);
    else
        exit("No se recibió dato principal");

    if (isset($datos['modo']))
        $modo = intval($datos['modo']);
    else
        exit("No se recibió el modo");
} catch (Exception $e) {
    exit("Error Inesperado, se recibio un id inválido" . $e->getMessage());
}

$obj = new articulos();
$res = new ResponseModel();

    $reg = $obj->obtenDatosArt($id);
    //var_dump($reg);
    if($reg==null) {
        $res->message="No existe el ID del artículo!";
        echo json_encode($res);
        return;
    }
    if($reg['Activo_Art']==0) {
        $res->message="El artículo NO esta Activo!";
        echo json_encode($res);
        return;
    }
    if($modo==1 && $reg['FechaPublicacion_Art']!=null) { //comparar que no este publicado
        $res->message="El artículo ya fue publicado!";
        echo json_encode($res);
        return;
    } else if($modo!=1 && $reg['FechaPublicacion_Art']==null) { //comparar que no este publicado
        $res->message="Otro Usuario ya Removio Fecha de publicación de este artículo!";
        echo json_encode($res);
        return;
    }

    $val = $obj->publicarArt($id,$modo); /// update articulos set fechapublicacion=?, id_modificad=?, Fecha_modifica=fechaactual Where id=$id and activo=1
    if ($val > 0) {
        $res->result = true;
        $res->title = 'Éxito!';
        if($modo==1)
            $res->message = 'El Artículo fue correctamente publicado!';
        else
            $res->message = 'El Artículo fue correctamente Despublicado!';
    }
    echo json_encode($res);

