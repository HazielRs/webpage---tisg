<?php 
	session_start();
	require_once (dirname(__FILE__) . "/../../clases/conexion.php");
	require_once (dirname(__FILE__) . "/../../clases/articulos.php");
    require_once (dirname(__FILE__) . "/../../clases/response.php");
	$ret= new ResponseModel();
    $status = '0';
	$IdArticulo = -1;
	try {
        if (isset($_POST['Id_Articulo']))
            $IdArticulo = intval($_POST['Id_Articulo']);
    }catch (Exception $e){
        exit("Error Inesperado, se recibio un valor inválido" . $e->getMessage());
    }

	$obj= new articulos();

    if(!isset($_POST['input-title-art'])){
        $ret-> message = 'El campo Título es obligatorio';
    echo json_encode($ret);
    return;
    }

	if($IdArticulo>0) { //es una modificacion
        if(!isset($_POST['status-art']))
        {
            $ret-> message = 'El estado del articulo es obligatorio';
            echo json_encode($ret);
            return;
        }
        $status = $_POST['status-art'];
        $datos = $obj->obtenDatosArt($IdArticulo);
        $datos['id_usuario']=$_POST['id_usuario'];
        $datos['Titulo_Art']=$_POST['input-title-art'];
        $datos['Tipo_Art']=$_POST['input-type-art'];
        $datos['Activo_Art']=$status;
        $datos['Introtext_Art']=$_POST['introtext-art'];
        $datos['Fulltext_Art']=$_POST['fulltext-art'];
        $datos['Tags_Art']=$_POST['tags-art'];

        $res=$obj->modificarArt($datos);
        if($res>0) {
            $ret->result=true;
            $ret->title='Exito';
            $ret->message='Los datos fueron actualizados!';
        }


    }

	else {

        if(!isset($_POST['input-type-art'])){
            $ret-> message = 'Es obligatorio escoger un tipo de artículo disponible.';
            echo json_encode($ret);
            return;
        }
        if(!($_POST['input-type-art']=="P" || $_POST['input-type-art']=="B")){
            $ret-> message = 'El tipo de articulo es inválido';
            echo json_encode($ret);
            return;
        }


        //definir array a devolver

	    try {
            $datos=array(
                "id_usuario" =>$_SESSION['iduser'],
                "Titulo_Art" =>$_POST['input-title-art'],
                "Tipo_Art" =>$_POST['input-type-art'],
                "Activo_Art" => 1,
                "Introtext_Art" => $_POST['introtext-art'],
                "Fulltext_Art" => $_POST['fulltext-art'],
                "Tags_Art" => $_POST['tags-art'],

            );
            //var_dump($datos);

            //Si es Nuevo.
            //1. verificar que el titulo no exista para el mismo autor
            $res=intval( $obj->ExisteTitulo($_POST['input-title-art']) );
            if ($res>0) {
                $ret->message='Este Título: '. $_POST['input-title-art'] . ' ya existe en la base de datos';
                echo json_encode($ret);
                return;
            }

            //2. Si ya existe, mostrar error que ya existe, caso contrario Grabar
            $res=$obj->registroArt($datos);
            //var_dump($res);
            if ($res>0) //si Id es Mayor a cero, se grabo
            {
                $ret->result =true;
                $ret->title='Artículo Grabado!';
                $ret->message="Haga clic en el botón para regresar a la pagina de Artículos...";
            }
            else { //Si hubo un error interno
                $ret->message='No se puedo acceder. Error desconocido de la base de datos';
            }
        }
        catch (Exception $e) {
            $ret->message = 'ERROR FATAL ->' . $e->getMessage();
        }

    }
echo json_encode($ret);
 ?>