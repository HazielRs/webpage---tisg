<?php
session_start();
require_once (dirname(__FILE__) . "/../../clases/conexion.php");
require_once (dirname(__FILE__) . "/../../clases/archivos.php");
require_once (dirname(__FILE__) . "/../../clases/response.php");

$dato=json_decode(file_get_contents('php://input'), true);
$obj= new archivos();
$res = new ResponseModel();
$id_delete = $obj->eliminaArchivo($dato['id']);
if ($id_delete>0){
    $res->result = true;
    $res->title = 'Éxito!';
    $res->message = 'Se eliminó el archivo satisfactoriamente!';
}
else{

    $res->title = 'Error!';
    $res->message = 'Error al eliminar el artículo!';
}
echo json_encode($res);