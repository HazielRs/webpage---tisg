function validar() {

    var newUsername, newEmail, newName, newSurname, newPassword, input_city, input_number, input_address, expresion;
    newUsername = document.getElementById("newUsername").value;
    newEmail = document.getElementById("newEmail").value;
    newName = document.getElementById("newName").value;
    newSurname = document.getElementById("newUsername").value;
    newPassword = document.getElementById("newPassword").value;
    input_city = document.getElementById("input_city").value;
    input_number = document.getElementById("input_number").value;
    input_address = document.getElementById("input_address").value;

    expresion = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (newUsername.length>20) {
        notify("Error", "El nombre de usuario es muy largo!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (newEmail.length>30) {
        notify("Error", "El correo es muy largo!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (!expresion.test(newEmail)){
        notify("Error", "El correo electrónico brindado no es válido", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (newName.length>20) {
        notify("Error", "Tu nombre es muy largo!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (newSurname.length>40) {
        notify("Error", "Tus apellidos son muy largos!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (newPassword.length>25) {
        notify("Error", "La contraseña no puede superar los 25 caracteres!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (input_city.length>20) {
        notify("Error", "El campo es muy largo!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (input_number.length>9) {
        notify("Error", "El numero telefónico es muy largo!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (input_address.length>40) {
        notify("Error", "Tu dirección es muy larga!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (isNaN(input_number)) {
        notify("Error", "El numero telefónico no es válido!", "danger", 'ni ni-fat-remove');
        return false;
    }
}
