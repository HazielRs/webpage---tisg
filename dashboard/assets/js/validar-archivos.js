function validar() {

    var titulo, descripcion;
    titulo = document.getElementById("titulo").value;
    descripcion= document.getElementById("descripcion").value;

    if (titulo.length>30) {
        notify("Error", "El titulo del archivo es muy largo!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (descripcion.length>150) {
        notify("Error", "La descripción del archivo es muy larga!", "danger", 'ni ni-fat-remove');
        return false;
    }
}