function validar() {

    var user_name, user_surname, user_number, user_password;
    user_name = document.getElementById("user_name").value;
    user_surname = document.getElementById("user_surname").value;
    user_number = document.getElementById("user_number").value;
    user_password = document.getElementById("user_password").value;

    if (user_name.length>20) {
        notify("Error", "El nombre de usuario es muy largo!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (user_surname.length>40) {
        notify("Error", "Los apellidos son muy largos!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (user_number.length>12) {
        notify("Error", "El numero telefónico es muy largo!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (isNaN(user_number)) {
        notify("Error", "El numero telefónico no es válido!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (user_password.length>25) {
        notify("Error", "La contraseña no puede superar los 25 caracteres!", "danger", 'ni ni-fat-remove');
        return false;
    }
}

