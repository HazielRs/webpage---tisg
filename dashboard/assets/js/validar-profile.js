function validar() {

    var input_first_name, input_last_name, input_password, repeat_password, input_address, input_city, input_number;
    input_first_name = document.getElementById("input_first_name").value;
    input_last_name = document.getElementById("input_last_name").value;
    input_password = document.getElementById("input_password").value;
    repeat_password = document.getElementById("repeat_password").value;
    input_address = document.getElementById("input_address").value;
    input_city = document.getElementById("input_city").value;
    input_number = document.getElementById("input_number").value;



    if (input_first_name.length>20) {
        notify("Error", "Tu nombre es muy largo!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (input_last_name.length>40) {
        notify("Error", "Tus apellidos son muy largos!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (input_password.length>25) {
        notify("Error", "La contraseña no puede superar los 25 caracteres!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (input_password !== repeat_password) {
        notify("Error", "Las contraseñas no coinciden!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (input_city.length>20) {
        notify("Error", "El campo es muy largo!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (input_number.length>12) {
        notify("Error", "El numero telefónico es muy largo!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (input_address.length>75) {
        notify("Error", "Tu dirección es muy larga!", "danger", 'ni ni-fat-remove');
        return false;
    }
    else if (isNaN(input_number)) {
        notify("Error", "El numero telefónico no es válido!", "danger", 'ni ni-fat-remove');
        return false;
    }

}
