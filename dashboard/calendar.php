  <!-- =========================================================
* Argon Dashboard v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)



=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 -->
<?php include ('procesos/security-login.php');
require_once (dirname(__FILE__) . "/../clases/conexion.php");

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Ministerio DOULOS - Administración">
  <title>Calendario</title>
  <!-- Favicon -->
  <link rel="icon" href="assets/img/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Page plugins -->
  <link rel="stylesheet" href="assets/vendor/fullcalendar/dist/fullcalendar.min.css">
  <link rel="stylesheet" href="assets/vendor/sweetalert2/dist/sweetalert2.min.css">
  <link rel="stylesheet" href="assets/vendor/animate.css/animate.min.css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body>
  <?php require_once ('Sidenav.php')?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <?php require_once ('Topnav.php')?>
    <!-- Header -->
    <!-- Header -->
    <div class="header header-dark bg-default pb-6 content__title content__title--calendar">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6">
              <h6 id="fullcalendar-title" class="h2 text-white d-inline-block mb-0">Full calendar</h6>
              <nav aria-label="breadcrumb" class="d-none d-lg-inline-block ml-lg-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href=../index.php><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="dashboard.php">Main</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Calendario</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 mt-3 mt-lg-0 text-lg-right">
              <a href="#" class="fullcalendar-btn-prev btn btn-sm btn-neutral">
                <i class="fas fa-angle-left"></i>
              </a>
              <a href="#" class="fullcalendar-btn-next btn btn-sm btn-neutral">
                <i class="fas fa-angle-right"></i>
              </a>
              <a href="#" class="btn btn-sm btn-neutral" data-calendar-view="month">Mes</a>
              <a href="#" class="btn btn-sm btn-neutral" data-calendar-view="basicWeek">Semana</a>
              <a href="#" class="btn btn-sm btn-neutral" data-calendar-view="basicDay">Día</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <!-- Fullcalendar -->
          <div class="card card-calendar">
            <!-- Card header -->
            <div class="card-header">
              <!-- Title -->
              <div id="calendar-year" class="h5 text-muted mb-1"></div>
              <div id="calendar-day" class="h3 mb-0"></div>
            </div>
            <!-- Card body -->
            <div class="card-body p-0">
              <div class="calendar" data-toggle="calendar" id="calendar"></div>
            </div>
          </div>
          <!-- Modal - Add new event -->
          <!--* Modal header *-->
          <!--* Modal body *-->
          <!--* Modal footer *-->
          <!--* Modal init *-->
          <div class="modal fade" id="new-event" tabindex="-1" role="dialog" aria-labelledby="new-event-label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-secondary" role="document">
              <div class="modal-content">
                <!-- Modal body -->
                <div class="modal-body">
                  <form class="" id="new-event--form">
                    <div class="form-group">
                      <label class="form-control-label">Titulo de Evento</label>
                      <input type="text" id="new-event--title" class="form-control form-control-alternative" placeholder="Titulo de Evento" value="">
                    </div>
                    <div class="form-group mb-0">
                      <label class="form-control-label d-block mb-3">Color</label>
                      <div class="btn-group btn-group-toggle btn-group-colors event-tag" data-toggle="buttons">
                        <label class="btn bg-info active"><input type="radio" name="event-tag" value="bg-info" autocomplete="off" checked></label>
                        <label class="btn bg-warning"><input type="radio" name="event-tag" value="bg-warning" autocomplete="off"></label>
                        <label class="btn bg-danger"><input type="radio" name="event-tag" value="bg-danger" autocomplete="off"></label>
                        <label class="btn bg-success"><input type="radio" name="event-tag" value="bg-success" autocomplete="off"></label>
                        <label class="btn bg-default"><input type="radio" name="event-tag" value="bg-default" autocomplete="off"></label>
                        <label class="btn bg-primary"><input type="radio" name="event-tag" value="bg-primary" autocomplete="off"></label>
                      </div>
                    </div>
                    <input id="new-event--start" type="hidden" class="" />
                    <input id="new-event--end" type="hidden" class="" />
                      <div class="form-group">
                          <label class="form-control-label">Descripción</label>
                          <textarea id="new-event--description" class="form-control form-control-alternative textarea-autosize" placeholder="Descripción de Evento"></textarea>
                          <i class="form-group--bar"></i>
                      </div>
                      <input id="edit-event--id" type="hidden" class="">
                  </form>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                  <button id="new-event--add" type="submit" class="btn btn-primary">Agregar</button>
                  <button type="button" class="btn btn-outline-danger ml-auto" data-dismiss="modal">Cancelar</button>
                </div>
              </div>
            </div>
          </div>
          <!-- Modal - Edit event -->
          <!--* Modal body *-->
          <!--* Modal footer *-->
          <!--* Modal init *-->
          <div class="modal fade" id="edit-event" tabindex="-1" role="dialog" aria-labelledby="edit-event-label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-secondary" role="document">
              <div class="modal-content">
                <!-- Modal body -->
                <div class="modal-body">
                  <form class="edit-event--form" id="evento-cal">
                    <div class="form-group">
                      <label class="form-control-label">Titulo de Evento</label>
                      <input id="edit-event--title" name="title-event" type="text" class="form-control form-control-alternative " placeholder="Titulo de Evento">
                    </div>
                    <div class="form-group">
                      <label class="form-control-label d-block mb-3">Color</label>
                      <div class="btn-group btn-group-toggle btn-group-colors edit-event-tag mb-0" data-toggle="buttons">
                        <label class="btn bg-info active"><input type="radio" name="event-tag" value="bg-info" autocomplete="off" checked></label>
                        <label class="btn bg-warning"><input type="radio" name="event-tag" value="bg-warning" autocomplete="off"></label>
                        <label class="btn bg-danger"><input type="radio" name="event-tag" value="bg-danger" autocomplete="off"></label>
                        <label class="btn bg-success"><input type="radio" name="event-tag" value="bg-success" autocomplete="off"></label>
                        <label class="btn bg-default"><input type="radio" name="event-tag" value="bg-default" autocomplete="off"></label>
                        <label class="btn bg-primary"><input type="radio" name="event-tag" value="bg-primary" autocomplete="off"></label>
                      </div>
                    </div>
                      <input id="edit-event--start" type="hidden" class="" />
                      <input id="edit-event--end" type="hidden" class="" />
                    <div class="form-group">
                      <label class="form-control-label">Descripción</label>
                      <textarea id="edit-event--description" class="form-control form-control-alternative textarea-autosize" placeholder="Descripción de Evento"></textarea>
                      <i class="form-group--bar"></i>
                    </div>
                    <input id="edit-event--id" type="hidden" class="">
                  </form>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                  <button class="btn btn-primary" data-calendar="update">Actualizar</button>
                  <button class="btn btn-danger" data-calendar="delete">Eliminar</button>
                  <button class="btn btn-link ml-auto" data-dismiss="modal">Cancelar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Optional JS -->
  <script src="assets/vendor/moment/min/moment.min.js"></script>
  <script src="assets/vendor/fullcalendar/dist/fullcalendar.min.js"></script>
  <script src="assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
  <script src="assets/vendor/moment/locale/es.js"></script>
  <script src="assets/vendor/fullcalendar/dist/locale/es.js"></script>
  <script src="assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>
  <!-- Argon JS -->
  <script src="assets/js/argon.js?v=1.2.0"></script>
<script>
    function edit(event){
        start = event.start.format('YYYY-MM-DD');
        if(event.end){
            end = event.end.format('YYYY-MM-DD');
        }else{
            end = start;
        }

        id =  event.id;

        Event = [];
        Event[0] = id;
        Event[1] = start;
        Event[2] = end;

        $.ajax({
            url: 'procesos/editEventDate.php',
            type: "POST",
            data: {Event:Event},
            success: function(rep) {
                if(rep == 'OK'){
                    notify('Exito', 'El evento se ha guardado correctamente!', 'success');
                    }
                else{
                    notify('Error', 'No se pudo guardar. Inténtalo de nuevo.', 'danger');
                }
            }
        });
    }
</script>
</body>

</html>