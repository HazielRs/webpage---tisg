<?php
session_start();
require_once (dirname(__FILE__) . '/clases/conexion.php');
$c = new conectar();
?>
<!doctype html>
<html lang="es_PE">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="img/favicon.png" type="image/png">
    <title>Ministerio Doulos - Id y hacer Discipulos a todas las naciones</title>
    <base href="<?php echo $c->urlbase;?>">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <link rel="stylesheet" href="vendors/popup/magnific-popup.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/my.css">
    <link rel="stylesheet" href="css/redessociales.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/filipelinhares/ress/master/dist/ress.min.css">
    <link rel="stylesheet" href="css/projects.css">
    <script src="js/jquery-3.3.1.min.js"></script>
</head>
<body>
<?php include ("snippets/navbar.php"); ?>

<!--================ CONTENIDO A PARTIR DE AQUI =================-->
<section id="projects">
        <!--Contenido de startbootstrap.com-->
    <div id="webcontent" class="webcontent">
        <!-- Page Wrap -->
        <div class="page-wrap">
            <div id="home-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide swiper-slide-one">
                            <div class="swiper-image" data-swiper-parallax-y="-20%">
                                <div class="swiper-image-inner swiper-image-left swiper-image-one">
                                    <h1>A <span class="emphasis">Breath</span>. <br><span>Of Fresh Air.</span></h1>
                                    <p>Chapter I, page XV</p>
                                </div>
                            </div>
                            <div class="swiper-image" data-swiper-parallax-y="35%">
                                <div class="swiper-image-inner swiper-image-right swiper-image-two">
                                    <p class="paragraph">
                                        A Prophet sat in the market-place and told the fortunes of all who cared to engage his services. Suddenly there came running up one who told him that his house had been broken into by thieves, and that they had made off with everything they could lay
                                        hands on.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide swiper-slide-two">
                            <div class="swiper-image" data-swiper-parallax-y="-20%">
                                <div class="swiper-image-inner swiper-image-left swiper-image-three">
                                    <h1>The <span class="emphasis">Drop</span>. <br><span>Of Eternal life.</span></h1>
                                    <p>Chapter II, page VII</p>
                                </div>
                            </div>
                            <div class="swiper-image" data-swiper-parallax-y="35%">
                                <div class="swiper-image-inner swiper-image-right swiper-image-four">
                                    <p class="paragraph">
                                        A thirsty Crow found a Pitcher with some water in it, but so little was there that, try as she might, she could not reach it with her beak, and it seemed as though she would die of thirst within sight of the remedy.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide swiper-slide-three">
                            <div class="swiper-image" data-swiper-parallax-y="-20%">
                                <div class="swiper-image-inner swiper-image-left swiper-image-five">
                                    <h1>A <span class="emphasis">Sense</span>. <br><span>Of Things to Come.</span></h1>
                                    <p>Chapter III, page XI</p>
                                </div>
                            </div>
                            <div class="swiper-image" data-swiper-parallax-y="35%">
                                <div class="swiper-image-inner swiper-image-right swiper-image-six">
                                    <p class="paragraph">
                                        Every man carries Two Bags about with him, one in front and one behind, and both are packed full of faults. The Bag in front contains his neighbours’ faults, the one behind his own. Hence it is that men do not see their own faults, but never fail to see
                                        those of others.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
        <!--================Instagram Area =================-->
        <?php require_once "snippets/InstaArea.php"; ?>
        <!--================End Instagram Area =================-->
    </div>
</section>
<br/>
<!--================ FIN DE CONTENIDO A PARTIR DE AQUI =================-->
<?php require_once "snippets/footer.php"; ?>

<!-- Optional JavaScript -->

<script src="js/bootstrap.min.js"></script>

<script src="vendors/lightbox/simpleLightbox.min.js"></script>
<script src="vendors/popup/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="vendors/counter-up/jquery.waypoints.min.js"></script>
<script src="vendors/counter-up/jquery.counterup.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/theme.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.jquery.min.js"></script>
<script src="js/projects.js"></script>
<!--My JS-->
<script src="js/proyectoweb.js"></script>

<script>
    /*Script de Autoejecucion cuando cargue la pagina*/
    $(function () {
        //marcar menu con clase active, dara color ROJO
        $("#nav_projects").addClass("active");

    });
</script>
</body>
</html>