var _isMobile=false;
function validarFormVacio(formulario){
		datos=$('#' + formulario).serialize();
		d=datos.split('&');
		vacios=0;
		for(i=0;i< d.length;i++){
				controles=d[i].split("=");
				if(controles[1]=="A" || controles[1]==""){
					vacios++;
				}
		}
		return vacios;
	}

	var EmailDomainSuggester = {

		domains: ["edu.pe","gob.pe","yahoo.com", "gmail.com", "google.com", "hotmail.com", "live.com", "msn.com", "facebook.com", "mail.com"],
		//bindTo: $('#type[email'),
		bindTo: $('input[type="email"]'),
		init: function () {
			this.addElements();
			this.bindEvents();
		},
	
		addElements: function () {
			// Create empty datalist
			this.datalist = $("<datalist />", {
				id: 'email-options'
			}).insertAfter(this.bindTo);
			// Corelate to input
			this.bindTo.attr("list", "email-options");
		},
	
		bindEvents: function () {
			this.bindTo.on("keyup", this.testValue);
		},
	
		testValue: function (event) {
			var el = $(this),
				value = el.val();
	
			// email has @
			// remove != -1 to open earlier
			if (value.indexOf("@") != -1) {
				value = value.split("@")[0];
				EmailDomainSuggester.addDatalist(value);
			} else {
				// empty list
				EmailDomainSuggester.datalist.empty();
			}
	
		},
	
		addDatalist: function (value) {
			var i, newOptionsString = "";
			for (i = 0; i < this.domains.length; i++) {
				newOptionsString +=
				  "<option value='" +
					value +
					"@" +
					this.domains[i] +
				  "'>";
			}
	
			// add new ones
			this.datalist.html(newOptionsString);
		}
	
	}

	function bloqueaSup() {
		$.blockUI({
			message: "Espere por favor ...",
			centerY: 0, 
			css: { top: "10px", left: "", right: "10px" } 
		});
	}
	
	
	function desbloquea() {
		setTimeout($.unblockUI,10);
	}
	function FunError(err) {
		if (err.responseText!= undefined)
			console.log(err.responseText);
		else
			console.log(err);
		alertify.error('Error: inesperado ... vea log');
		return false;
	}

/** Trata de seleccionar un Elemento dentro de un Select */
function setValueSelect(selectId, value) {
    var selectObject = document.getElementById(selectId);
    for (var index = 0; index < selectObject.options.length; index++) {
        if (selectObject[index].value === value) {
            selectObject.selectedIndex = index;
            return;
        }
    }
}

function setTextoInSelect(selectId, value) {
    var selectObject = document.getElementById(selectId);
    for (var index = 0; index < selectObject.options.length; index++) {
        if (selectObject.options[index].text === value) {
            selectObject.options.selectedIndex = index;
            return true;
        }
    }
    return false;
}

function buscaParcialTextoInSelect(selectId, value) {
    var selectObject = document.getElementById(selectId);
    for (var index = 0; index < selectObject.options.length; index++) {
        if (selectObject.options[index].text.includes(value)) {
            selectObject.options.selectedIndex = index;
            return true;
        }
    }
    return false;
}
/* devuelve verdadero o falso, si un elemento se encuentra dentro de un Select */
function buscarTextoInSelect(selectId, value) {
    var selectObject = document.getElementById(selectId); 
    for (var index = 0; index < selectObject.options.length; index++) {
        if (selectObject.options[index].text === value)
            return true;
    }
    return false;
}	

/* Eliminar Controles typeahead */
$('button[name="btnRemoveValor"]').click(
    function(e) {
        e.preventDefault();
        var c1 = this.getAttribute('data-val-id1');
        var c2 = this.getAttribute('data-val-id2');
        if (c1.toLowerCase().indexOf('id', 0) >= 0)
            $("#" + c1).val("-1");
        else
            $("#" + c1).val("");
        if (c2.toLowerCase().indexOf('id', 0) >= 0)
            $("#" + c2).val("-1");
        else
            $("#" + c2).val("");
    });

function FunEnviaForm(button, isCerrarAutomaticamente, functionaEjecutarSiOkAlFinalizar, functionaEjecutarSiErrorAlFinalizar) { //isCerrarAutomaticamente add 18 oct 17
	var idform = button.attr("form");
	isCerrarAutomaticamente = typeof isCerrarAutomaticamente !== 'undefined' ? isCerrarAutomaticamente : false;
	functionaEjecutarSiOkAlFinalizar = typeof functionaEjecutarSiOkAlFinalizar !== 'undefined' ? functionaEjecutarSiOkAlFinalizar : undefined;
	functionaEjecutarSiErrorAlFinalizar = typeof functionaEjecutarSiErrorAlFinalizar !== 'undefined' ? functionaEjecutarSiErrorAlFinalizar : undefined;
	var form;
	var respuestaJson = false;
	if (idform === undefined) {
		form = button.closest("form");
	} else {
		form = $("#"+idform);
	}
	var url = form.attr("action");
	// Creamos un div que bloqueara toodo el formulario
	var block = $('<div class="block-loading" />');
	form.prepend(block);

	// En caso de que haya habido un mensaje de alerta
	$(".alert", form).remove();

	// Para los formularios que tengan CKupdate
	//if (form.hasClass("CKupdate")) CKupdate();

	form.ajaxSubmit({
		dataType: "JSON",
		type: "POST",
		url: url,
		success: function (r) {
			block.remove();
			respuestaJson = r.response;
			if (r.response) {
				if (isCerrarAutomaticamente && functionaEjecutarSiOkAlFinalizar!=undefined) {
					setTimeout(functionaEjecutarSiOkAlFinalizar,100,r.result, r.response, r.message);
					return r.result; //salir inmediatamente, control se traspasa al emisor, devuelve el Id Nuevo que se genero
				}
				swal({
					title: "Procesado",
					text: r.message + ". Este cuadro se reiniciara en 3 segundos",
					type: "success",
					confirmButtonText: "Submit",
					showLoaderOnConfirm: true,
					timer: 3000,
					allowOutsideClick: false
				}).then(
					function() { //Si hace Click en Aceptar Revisa href
						if (r.href != null) {
							if (r.href === "self")
								window.location.reload(true);
							else window.location.href = r.href;
						}
					},
					// handling the promise rejection
					function (dismiss) {
						if (dismiss === "timer") {
							//console.log('I was closed by the timer')
							// Redireccionar
							if (r.href != null) {
								if (r.href === "self")
									window.location.reload(true);
								else
									window.location.href = r.href;
							}
						}
					});
				//if(!button.data('reset') != undefined) {
				//    if (button.data('reset')) form.reset();
				//}
				//else {
				//    form.find('input:file').val('');
				//}
			}  //Fin de si devolvio Mensaje de OK


			// Mostrar mensaje
			if (r.message != null) {
				if (r.message.length > 0) {
					var css = "";
					if (r.response) css = "alert-success";
					else css = "alert-danger";
					var message = '<div class="alert ' + css + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + r.message + '</div>';
					form.prepend(message);
				}
			}

			if (isCerrarAutomaticamente && !r.response) { //Add 15Dic17 --para GuiaRemision, (Oddo, RRHH - 1ero Mapear, luego procesar)
				if (functionaEjecutarSiErrorAlFinalizar !== undefined)
					setTimeout(functionaEjecutarSiErrorAlFinalizar, 100, r.result);
				return r.result; //salir inmediatamente, control se traspasa al emisor, devuelve el Id Nuevo que se genero
			}

			// Validaciones
			if (r.Validations !== null) {
				for (var k in r.Validations) {
					var vmessage = r.Validations[k];
					form.find("[data-key='" + k + "']").text(vmessage);
				}
			}

			// Ejecutar funciones
			if (r.function != null) {
				setTimeout(r.function, 0);
			}

		},
		error: function (jqXHR, textStatus, errorThrown) {
			block.remove();
			form.prepend('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + errorThrown + '| <b> ' + textStatus + '</b></div>');
		}
	});
	return respuestaJson;
}

function validaSoloNumeros(e) {
    /*  En input onkeypress="return validaSoloNumeros(event) */
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

/** **************************************************************************************
 * Funciones utiles de SUNAT
 ** **************************************************************************************/
function esnulo(campo) { return (campo == null || campo === ""); }
function esnumero(campo) { return (!(isNaN(campo))); }
function eslongrucok(ruc) { return (ruc.length === 11); }
function trim(cadena) {
    var cadena2 = "";
    len = cadena.length;
    for (let i = 0; i <= len ; i++) if (cadena.charAt(i) != " ") { cadena2 += cadena.charAt(i); }
    return cadena2;
}
function valruc(valor) {
    valor = trim(valor);
    if (esnumero(valor)) {
        var resto;
        var suma;
        var i;
        if (valor.length === 8) {
            suma = 0;
            for (i = 0; i < valor.length - 1; i++) {
                digito = valor.charAt(i) - "0";
                if (i == 0) suma += (digito * 2);
                else suma += (digito * (valor.length - i));
            }
            resto = suma % 11;
            if (resto === 1) resto = 11;
            if (resto + (valor.charAt(valor.length - 1) - "0") === 11) {
                return true;
            }
        } else if (valor.length === 11) {
            suma = 0;
            x = 6;
            for (i = 0; i < valor.length - 1; i++) {
                if (i == 4) x = 8;
                digito = valor.charAt(i) - "0";
                x--;
                if (i == 0) suma += (digito * x);
                else suma += (digito * x);
            }
            resto = suma % 11;
            resto = 11 - resto;

            if (resto >= 10) resto = resto - 10;
            if (resto == valor.charAt(valor.length - 1) - "0") {
                return true;
            }
        }
    }
    return false;
}
function esrucok(ruc) {
    return (!(esnulo(ruc) || !esnumero(ruc) || !eslongrucok(ruc) || !valruc(ruc)));
}
function esTeclaNumero(e) {
    var valid = "0123456789";
    var key = String.fromCharCode(event.keyCode);
    if (valid.indexOf("" + key) == "-1") return false;
}
/** FIN funciones utiles SUNAT ************************************************************/
$(".up").click(function () {
	$("body, html").animate({ scrollTop: "0px" }, 200);
});

$(window).scroll(function () {

	if ($(window).scrollTop() > (_isMobile ? 100 : 200)) {
		$(".up").fadeIn("300");
	} else {
		$(".up").fadeOut("300");
	}
	if ($(document).scrollTop() > 150) {
		$('.logo').height(40);

	}
	else {
		$('.logo').height(80);
	}
});

$(".up").click(function () {
	$("body, html").animate({ scrollTop: "0px" }, 200);
});