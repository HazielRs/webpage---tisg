var _isMobile=false;

//Se ejecuta cuando se cargo toda la pagina web.
$(document).ready(function () {
    if (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase())) {
        _isMobile = true;
    }
    if(document.getElementById('projects') || document.getElementById('events')
        || document.getElementById('contact')
        || document.getElementById('index'))
    {
        $("#webcontent").css('margin-top', document.querySelector("header").offsetHeight+ "px");
        //evitar que contenido principal se solape con banner de MENU.
    }
//Esconder marcador de Boton SUBIR ARRIBA por defecto
    $(".up").hide();
});

    $(".up").click(function () {
    $("body, html").animate({ scrollTop: "0px" }, 200);
    });

/* Codigo de Evento, cuando se hacer Scroll con Mouse o Barra de desplazamiento del navegador */
    $(window).scroll(function () {

    if ($(window).scrollTop() > (_isMobile ? 100 : 200)) {
        $(".up").fadeIn("300");
    } else {
        $(".up").fadeOut("300");
    }
    /*if ($(document).scrollTop() > 150) {
        $('.logo').height(40);

    }
    else {
        $('.logo').height(80);
    }*/
    });

/*Codigo que se ejecuta cuando se redimenciona ventana del browser*/
    $(window).resize(function () {
    $("#webcontent").css('margin-top', document.querySelector("header").offsetHeight+ "px");
    });