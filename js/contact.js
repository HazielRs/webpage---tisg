$(document).ready(function(){
    
    (function($) {
        "use strict";

    
    jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value)
    }, "Escribe la respuesta correcta -_-");

    // validate contactForm form
    $(function() {
        $('#contactForm').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 5
                },
                subject: {
                    required: true,
                    minlength: 5
                },
                number: {
                    required: true,
                    minlength: 9
                },
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true,
                    minlength: 20
                }
            },
            messages: {
                name: {
                    required: "Vamos, no tienes un nombre?",
                    minlength: "Tu nombre debe consisitir de al menos 5 caracteres"
                },
                subject: {
                    required: "No tienes algún Tema?",
                    minlength: "Tu  tema debe consistir de al menos 5 caracteres"
                },
                number: {
                    required: "Requerimos tu móvil para contactarte",
                    minlength: "Tu número debe consistir de al menos 9 caracteres"
                },
                email: {
                    required: "Si no hay email, no hay mensaje :)"
                },
                message: {
                    required: "Tienes que escribir algo para enviar este mensaje...",
                    minlength: "¿Eso es todo? ¿En serio?"
                }
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    type:"POST",
                    data: $(form).serialize(),
                    url:"contact_process.php",
                    success: function() {
                        $('#contactForm :input').attr('disabled', 'disabled');
                        $('#contactForm').fadeTo( "slow", 1, function() {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor','default');
                            $('#success').fadeIn()
                            $('.modal').modal('hide');
		                	$('#success').modal('show');
                        })
                    },
                    error: function() {
                        $('#contactForm').fadeTo( "slow", 1, function() {
                            $('#error').fadeIn()
                            $('.modal').modal('hide');
		                	$('#error').modal('show');
                        })
                    }
                })
            }
        })
    })
        
 })(jQuery)
})