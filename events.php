<?php
session_start();
require_once (dirname(__FILE__) . '/clases/conexion.php');
$c = new conectar();
?>
<!doctype html>
<html lang="es_PE">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="img/favicon.png" type="image/png">
    <title>Ministerio Doulos - Id y hacer Discipulos a todas las naciones</title>
    <base href="<?php echo $c->urlbase;?>">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">-->
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/my.css">
    <link rel="stylesheet" href="css/redessociales.css">
    <link rel="stylesheet" href="css/events.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/835ac9bcc2f5ac466178e6578/c48fd358b3ec665fb141d17c7.js");</script>
</head>
<body>
<?php include ("snippets/navbar.php"); ?>
<div id="events">
    <div id="webcontent" class="webcontent">
<!--================ CONTENIDO A PARTIR DE AQUI =================-->
        <section class="service_area" style="margin-top: 10em;">
            <div class="container box_1620">
                <div class="main_title">
                    <h2>Nuestros Eventos</h2>
                    <p>Algunos de nuestros eventos próximos en nuestro local. Visítanos y únete para ser parte de nuestros eventos y de nuestra familia!</p>
                </div>
                <div class="blog-card">
                    <div class="meta">
                        <div class="photo" style="background-image: url(https://storage.googleapis.com/chydlx/codepen/blog-cards/image-1.jpg)"></div>
                        <ul class="details">
                            <li class="author"><a href="#">John Doe</a></li>
                            <li class="date">Aug. 24, 2015</li>
                            <li class="tags">
                                <ul>
                                    <li><a href="#">Learn</a></li>
                                    <li><a href="#">Code</a></li>
                                    <li><a href="#">HTML</a></li>
                                    <li><a href="#">CSS</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="description">
                        <h1>Learning to Code</h1>
                        <h2>Opening a door to the future</h2>
                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis! Neque ad aliquam facilis numquam. Veritatis, sit.</p>
                        <p class="read-more">
                            <a href="#">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="blog-card alt">
                    <div class="meta">
                        <div class="photo" style="background-image: url(https://storage.googleapis.com/chydlx/codepen/blog-cards/image-2.jpg)"></div>
                        <ul class="details">
                            <li class="author"><a href="#">Jane Doe</a></li>
                            <li class="date">July. 15, 2015</li>
                            <li class="tags">
                                <ul>
                                    <li><a href="#">Learn</a></li>
                                    <li><a href="#">Code</a></li>
                                    <li><a href="#">JavaScript</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="description">
                        <h1>Mastering the Language</h1>
                        <h2>Java is not the same as JavaScript</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis! Neque ad aliquam facilis numquam. Veritatis, sit.</p>
                        <p class="read-more">
                            <a href="#">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="blog-card">
                    <div class="meta">
                        <div class="photo" style="background-image: url(https://storage.googleapis.com/chydlx/codepen/blog-cards/image-1.jpg)"></div>
                        <ul class="details">
                            <li class="author"><a href="#">John Doe</a></li>
                            <li class="date">Aug. 24, 2015</li>
                            <li class="tags">
                                <ul>
                                    <li><a href="#">Learn</a></li>
                                    <li><a href="#">Code</a></li>
                                    <li><a href="#">HTML</a></li>
                                    <li><a href="#">CSS</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="description">
                        <h1>Learning to Code</h1>
                        <h2>Opening a door to the future</h2>
                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis! Neque ad aliquam facilis numquam. Veritatis, sit.</p>
                        <p class="read-more">
                            <a href="#">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="blog-card alt">
                    <div class="meta">
                        <div class="photo" style="background-image: url(https://storage.googleapis.com/chydlx/codepen/blog-cards/image-2.jpg)"></div>
                        <ul class="details">
                            <li class="author"><a href="#">Jane Doe</a></li>
                            <li class="date">July. 15, 2015</li>
                            <li class="tags">
                                <ul>
                                    <li><a href="#">Learn</a></li>
                                    <li><a href="#">Code</a></li>
                                    <li><a href="#">JavaScript</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="description">
                        <h1>Mastering the Language</h1>
                        <h2>Java is not the same as JavaScript</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis! Neque ad aliquam facilis numquam. Veritatis, sit.</p>
                        <p class="read-more">
                            <a href="#">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="blog-card">
                    <div class="meta">
                        <div class="photo" style="background-image: url(https://storage.googleapis.com/chydlx/codepen/blog-cards/image-1.jpg)"></div>
                        <ul class="details">
                            <li class="author"><a href="#">John Doe</a></li>
                            <li class="date">Aug. 24, 2015</li>
                            <li class="tags">
                                <ul>
                                    <li><a href="#">Learn</a></li>
                                    <li><a href="#">Code</a></li>
                                    <li><a href="#">HTML</a></li>
                                    <li><a href="#">CSS</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="description">
                        <h1>Learning to Code</h1>
                        <h2>Opening a door to the future</h2>
                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis! Neque ad aliquam facilis numquam. Veritatis, sit.</p>
                        <p class="read-more">
                            <a href="#">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="blog-card alt">
                    <div class="meta">
                        <div class="photo" style="background-image: url(https://storage.googleapis.com/chydlx/codepen/blog-cards/image-2.jpg)"></div>
                        <ul class="details">
                            <li class="author"><a href="#">Jane Doe</a></li>
                            <li class="date">July. 15, 2015</li>
                            <li class="tags">
                                <ul>
                                    <li><a href="#">Learn</a></li>
                                    <li><a href="#">Code</a></li>
                                    <li><a href="#">JavaScript</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="description">
                        <h1>Mastering the Language</h1>
                        <h2>Java is not the same as JavaScript</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eum dolorum architecto obcaecati enim dicta praesentium, quam nobis! Neque ad aliquam facilis numquam. Veritatis, sit.</p>
                        <p class="read-more">
                            <a href="#">Read More</a>
                        </p>
                    </div>
                </div>
    <!--                    <div class="portfolio_right_text">-->
    <!--                        <h4>Título de evento</h4>-->
    <!--                        <p>Descripción del evento, ya sea pasado o por darse</p>-->
    <!--                        <ul class="list">-->
    <!--                            <li><span>Lugar</span>: Av. Paseo de la Cultura 505</li>-->
    <!--                            <li><span>Hora</span>: 05:00pm</li>-->
    <!--                            <li><span>Fecha</span>:  01/01/2020</li>-->
    <!--                            <br/>-->
    <!--                            <a href="#" class="genric-btn primary small">Asistiré!</a>-->
    <!--                        </ul>-->
    <!--                        <ul class="list social_details">-->
    <!--                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
    <!--                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
    <!--                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>-->
    <!--                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>-->
    <!--                        </ul>-->
    <!--                    </div>-->
            </div>
        </section>
        <!--================Instagram Area =================-->
        <?php require_once "snippets/InstaArea.php"; ?>
        <!--================End Instagram Area =================-->
    <!--================ FIN DE CONTENIDO A PARTIR DE AQUI =================-->
    </div>
</div>
<br/>
    <?php require_once "snippets/footer.php"; ?>

<!-- Optional JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="vendors/lightbox/simpleLightbox.min.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/theme.js"></script>
<!--My JS-->
<script src="js/proyectoweb.js"></script>


<script>
        /*Script de Autoejecucion cuando cargue la pagina*/
        $(function () {
            //marcar menu con clase active, dara color ROJO
            $("#nav_eventos").addClass("active");

        });
    </script>
</body>
</html>
