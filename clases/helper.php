<?php

    class helper {
        public function is_valid_email($email)
        {
            $result = (false !== filter_var($email, FILTER_VALIDATE_EMAIL));
            $user='';
            $domain ='';
            if ($result)
            {
                list($user, $domain) = preg_split("~@~", $email);
                $result = checkdnsrr($domain, 'MX');
            }
            return $result;
        }

    }