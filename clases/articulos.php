<?php 

	class articulos{

        public function __construct(){
        setlocale(LC_ALL, 'es_PE', 'es');
        date_default_timezone_set('America/Lima');
        }

		public function registroArt($datos){
            $c= new conectar();
            $db=$c->conexionPDO();
			$fecha=date('Y-m-d h:i');
            $iduser = $_SESSION['iduser'];
			$visitas=0;
			$status=1;
            $sql="";
            $insertID=-1;
			try {
            $sql="INSERT INTO articulos (
                Tipo_Art, Tags_Art, id_usuario, FechaCreacion_Art,
                Visitas_Art,Titulo_Art,Introtext_Art,
                Fulltext_Art,Activo_Art)
            VALUES (:tipo_Art, :tags_Art, :id_usuario, :fechaCreacion_Art,
                :visitas_Art, :titulo_Art, :introtext_Art,
                :fulltext_Art, :activo_Art);";

                $db->beginTransaction(); // also helps speed up your inserts.
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':tipo_Art', $datos['Tipo_Art']);
                $stmt->bindParam(':tags_Art', $datos['Tags_Art']);
                $stmt->bindParam(':id_usuario', $iduser);
                $stmt->bindParam(':fechaCreacion_Art', $fecha);
                $stmt->bindParam(':visitas_Art', $visitas);
                $stmt->bindParam(':titulo_Art', $datos['Titulo_Art']);
                $stmt->bindParam(':introtext_Art', $datos['Introtext_Art']);
                $stmt->bindParam(':fulltext_Art', $datos['Fulltext_Art']);
                $stmt->bindParam(':activo_Art',$status, PDO::PARAM_INT);
                //var_dump($datos);
                $res = $stmt->execute();
                //$stmt = $db->query('SHOW WARNINGS');
                //var_dump($res);
                if($res) {
                    $insertID=$db->lastInsertId();
                    $db->commit();
                }
                return $insertID;
            }
            catch(PDOException $e)
            {
                //var_dump($e);
                return "Error: " . $e->getMessage();
            }
		}

        public function obtenDatosArt($IdArticulo){
            $c=new conectar();
            $db=$c->conexionPDO();
            try {
                $sql = "SELECT IdArticulo, Tipo_Art, Tags_Art, id_usuario, FechaCreacion_Art, 
                    Visitas_Art, FechaPublicacion_Art, Titulo_Art, Introtext_Art, Fulltext_Art, 
                    UrlImg_Art, FechaModificacion_Art, Idusuariomodifica_Art, Activo_Art
					from articulos
					where IdArticulo=$IdArticulo";
            $datos=$db->query($sql)->fetch(PDO::FETCH_ASSOC);
                if ($datos['UrlImg_Art']=='')
                    $datos['UrlImg_Art']=$c->urlbase . 'img/blog/'. $c->imgdefault;
                if (substr($datos['UrlImg_Art'], 0,5)!="http:"){
                    $datos['UrlImg_Art']=$c->urlbase . $datos['UrlImg_Art'];
                }
                return $datos;

            }catch (Exception $e){
                return $e->getMessage();
            }
        }

        public function RegistroVacio(){
            $c=new conectar();
            $urldefault=$c->urlbase . 'img/blog/' . $c->imgdefault;
            $fecha=date('Y-m-d h:i');
            $IduserCrea = $_SESSION['iduser'];
            //var_dump($iduserCrea);
            $ret=array(
                'IdArticulo'=>-1,
                'Tipo_Art'=> "B",
                'Tags_Art'=> "",
                'id_usuario'=> "",
                'FechaCreacion_Art'=> $fecha,
                'Visitas_Art'=> 0,
                'FechaPublicacion_Art'=> "",
                'Titulo_Art'=> "",
                'Introtext_Art'=> "",
                'Fulltext_Art'=> "",
                'UrlImg_Art'=> "$urldefault",
                'FechaModificacion_Art'=> "",
                'Idusuariomodifica_Art'=> $IduserCrea,
                'Activo_Art'=> 1,
            );
            return $ret;
        }

        public function ExisteTitulo($titulo){
            $nrows=0;
            $c= new conectar();
            $db=$c->conexionPDO();

            $sql="SELECT count(*) FROM articulos where Activo_Art=1 and Titulo_Art=:titulo_art";

            try {
                $stmt = $db->prepare($sql);
                $stmt->bindParam("titulo_art", $titulo,PDO::PARAM_STR) ;
                $stmt->execute(); //true o false
                //$nrows=$stmt->rowCount();
                $nrows=$stmt->fetchColumn();
            }
            catch(PDOException $e)
            {
                //var_dump($e);
                $nrows=-1;
                //return "Error: " . $e->getMessage();
            }

            //$registro = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
            return $nrows;
        }

        //Devuelve todos los registros de articulos
        public function obtenAllDatos(){
            $c=new conectar();
            $db=$c->conexionPDO();
            try {
                $sql = "SELECT a.IdArticulo, a.Titulo_Art, CONCAT(u.nombre, ' ', u.apellido) as author,
                        a.FechaCreacion_Art, a.Visitas_Art, a.Activo_Art
                        FROM articulos AS a
                        LEFT JOIN usuarios AS u ON u.id_usuario = a.id_usuario
                        ORDER BY a.FechaCreacion_Art DESC";
                return $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);

            }catch (Exception $e){
                return $e->getMessage();
            }
        }

        public function obtenAllRegistros(){
            $c=new conectar();
            $db=$c->conexionPDO();
            try {
                $sql = "SELECT count(*) as cantidad
                        FROM articulos WHERE Activo_Art=1";
                $stmt = $db->prepare($sql);

                $stmt->execute(); //true o false
                $nrows=$stmt->fetchColumn();
            }catch (Exception $e){
                $nrows=-1;
                return $e->getMessage();
            }
            return $nrows;
        }
        public function obtenAllRegistrosFromPagina($from,$cantidad){
            $c=new conectar();
            $db=$c->conexionPDO();
            if(!is_numeric($from))
                $from = 1;
            if(!is_numeric($cantidad))
                $cantidad=1;

            try {
                $urlDefault = $c->urlbase . 'img/'. $c->imgdefault ;
                //var_dump($urlDefault);
                $sql = "SELECT a.IdArticulo, a.Tipo_Art, a.Tags_Art, 
                    a.Visitas_Art, a.FechaPublicacion_Art, a.Titulo_Art, a.Introtext_Art,  
                    COALESCE(case when a.UrlImg_Art='' then null else a.UrlImg_Art end,'$urlDefault') as UrlImg_Art, 
                    a.Activo_Art, CONCAT(u.nombre, ' ' , u.apellido) as author
					FROM articulos as a
                    left join usuarios as u on u.id_usuario=a.id_usuario
                    WHERE a.Activo_Art=1 
                    AND Year(COALESCE(a.FechaPublicacion_Art,MakeDate(1900,1)))>1900
                    order by a.FechaPublicacion_Art desc
                    LIMIT $from, $cantidad"; //a.Fulltext_Art,

                /*$stmt = $db->prepare($sql);
                $stmt->bindParam(":urldefault", $urlDefault, PDO::PARAM_STR);
                $stmt->bindParam(":from", $from,PDO::PARAM_INT);
                $stmt->bindParam(":cantidad", $cantidad,PDO::PARAM_INT);*/
                $stmt=$db->query($sql);
                //return $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                //$stmt->execute();
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }catch (Exception $e){
                return $e->getMessage();
            }
        }
		public function modificarArt($datos){
            $c= new conectar();
            $db=$c->conexionPDO();
            $fecha=date('Y-m-d h:i:s');
            $Id=$datos['IdArticulo'];
            //var_dump($datos);
            $Idusermodifica = $_SESSION['iduser'];
            $fechapublicacion=$datos['FechaPublicacion_Art'] == '' ? null : $datos['FechaPublicacion_Art'];
            $active = false;

            if($datos['Activo_Art']=='1')
                $active = true;
            //var_dump($active);
            $sql="UPDATE articulos SET Tipo_Art=:tipo_Art, Tags_Art=:tags_Art, id_usuario=:id_usuario,
                    FechaPublicacion_Art=:fechaPublicacion_Art, Titulo_Art=:titulo_Art, Introtext_Art=:introtext_Art, Fulltext_Art=:fulltext_Art, 
                    UrlImg_Art=:urlImg_Art, FechaModificacion_Art=:fechaModificacion_Art, Idusuariomodifica_Art=:idusuariomodifica_Art, Activo_Art=:activo_Art 
                  WHERE IdArticulo=:idArticulo";

            try {
                $db->beginTransaction(); // also helps speed up your inserts.
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':tipo_Art', $datos['Tipo_Art']);
                $stmt->bindParam(':tags_Art', $datos['Tags_Art']);
                $stmt->bindParam(':id_usuario', $datos['id_usuario']);
                $stmt->bindParam(':fechaPublicacion_Art', $fechapublicacion);
                $stmt->bindParam(':titulo_Art', $datos['Titulo_Art']);
                $stmt->bindParam(':introtext_Art', $datos['Introtext_Art']);
                $stmt->bindParam(':fulltext_Art', $datos['Fulltext_Art']);
                $stmt->bindParam(':urlImg_Art', $datos['UrlImg_Art']);
                $stmt->bindParam(':fechaModificacion_Art', $fecha);
                $stmt->bindParam(':idusuariomodifica_Art', $Idusermodifica);
                $stmt->bindParam(':activo_Art', $active, PDO::PARAM_BOOL);
                $stmt->bindParam(':idArticulo', $Id);

                $res = $stmt->execute();
//                $st = $db->query('SHOW WARNINGS');
                if($res) {
                    $db->commit();
                    return $Id;
                }
                else {
                    return -1;
                }
            }
            catch(PDOException $e)
            {
                //var_dump($e);
                return "Error: " . $e->getMessage();
            }
		}

        public function ActualizarImgArt($id, $UrlImg_Art){
            $c= new conectar();
            $db=$c->conexionPDO();
            $fecha=date('Y-m-d h:i:s');

            $Idusermodifica = $_SESSION['iduser'];
            //var_dump($iduserCrea);
            $sql="UPDATE articulos SET UrlImg_Art=?, FechaModificacion_Art=?, Idusuariomodifica_Art=?
                  WHERE IdArticulo=? and Activo_Art=1";
            try {
                $db->beginTransaction(); // also helps speed up your inserts.
                $stmt = $db->prepare($sql);
                $res = $stmt->execute(array(
                    $UrlImg_Art,
                    $fecha,
                    $Idusermodifica,
                    $id,
                ));
                if($res) {
                    $db->commit();
                    return $id;
                }
                else
                    return -1;
            }
            catch(PDOException $e)
            {
                //var_dump($e);
                return "Error: " . $e->getMessage();
            }
        }

		public function eliminarArt($IdArticulo){
            $c= new conectar();
            $db=$c->conexionPDO();


            $sql="DELETE from articulos where IdArticulo=:id";
            try {
                $db->beginTransaction(); // also helps speed up your inserts.
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':id', $IdArticulo);
                $res = $stmt->execute();
                if($res) {
                    $db->commit();
                    return $IdArticulo;
                }
                else
                    return -1;
            }
            catch(PDOException $e)
            {
                //var_dump($e);
                return "Error: " . $e->getMessage();
            }
		}

        public function publicarArt($id,$modo){
            $c= new conectar();

            $db=$c->conexionPDO();
            $Idusermodifica = $_SESSION['iduser'];
            $fechapublicacion=date('Y-m-d h:i:s');
            $fechamodificacion=date('Y-m-d h:i:s');
            $sql = "UPDATE articulos SET 
                    FechaPublicacion_Art=:fechaPublicacion_Art, 
                    FechaModificacion_Art=:fechaModificacion_Art, 
                    Idusuariomodifica_Art=:idusuariomodifica_Art 
                  WHERE IdArticulo=:id AND Activo_Art=1";
            if($modo!=1) {
                $fechapublicacion=null;
            }

            try {
                $db->beginTransaction(); // also helps speed up your inserts.
                $stmt = $db->prepare($sql);

                $stmt->bindParam(':fechaPublicacion_Art', $fechapublicacion);
                if($modo==1)
                    $stmt->bindParam(':fechaModificacion_Art', $fechamodificacion);
                else
                    $stmt->bindParam(':fechaModificacion_Art', $fechamodificacion, PDO::PARAM_NULL);
                $stmt->bindParam(':idusuariomodifica_Art', $Idusermodifica);
                $stmt->bindParam(':id', $id);

                $res = $stmt->execute();
                if($res) {
                    $db->commit();
                    return $id;
                }
                else {
                    return -1;
                }
            }
            catch(PDOException $e)
            {
                //var_dump($e);
                return "Error: " . $e->getMessage();
            }
        }
	}

 ?>