<?php 

	class calendar{

        public function __construct(){
        setlocale(LC_ALL, 'es_PE', 'es');
        date_default_timezone_set('America/Lima');
        }

		public function registro($datos){
            $c= new conectar();
            $db=$c->conexionPDO();
            $iduser = $_SESSION['iduser'];
            $insertID=-1;
            //$start = date('Y-m-d', $datos['start']);
            //$end = date('Y-m-d', $datos['end']);
            //var_dump($datos);
			try {
            $sql="INSERT INTO calendario (title,description,className,start,end,allDay,Iduser)
            VALUES (:title, :description,:className,:start,:end,:allDay,:iduser)";

                $db->beginTransaction(); // also helps speed up your inserts.
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':title', $datos['title'], PDO::PARAM_STR );
                $stmt->bindParam(':description', $datos['description'], PDO::PARAM_STR );
                $stmt->bindParam(':className', $datos['className'], PDO::PARAM_STR );
                $stmt->bindParam(':start', $datos['start'], PDO::PARAM_STR );
                $stmt->bindParam(':end', $datos['end'], PDO::PARAM_STR);
                $stmt->bindParam(':allDay', $datos['allDay']);
                $stmt->bindParam(':iduser', $iduser, PDO::PARAM_INT);
                //var_dump($datos);
                $res = $stmt->execute();
                //$stmt = $db->query('SHOW WARNINGS');
                //var_dump($res);
                if($res) {
                    $insertID=$db->lastInsertId();
                    $db->commit();
                }
                return $insertID;
            }
            catch(PDOException $e)
            {
                //var_dump($e);
                return "Error: " . $e->getMessage();
            }
		}

        public function obtenDatos($idCalendar){

            $c=new conectar();
            $db=$c->conexionPDO();
            try {
                $sql = "SELECT id,title,description,className,start,end,allDay,Iduser
					from calendar
					where id=$idCalendar";
                return $db->query($sql)->fetch(PDO::FETCH_ASSOC);
            }catch (Exception $e){
                return $e->getMessage();
            }
        }

        //Devuelve todos los registros de articulos
        public function obtenAllDatosForMes($start, $end){
            $c=new conectar();
            $db=$c->conexionPDO();
            /*$start = strtotime($start);
            $end=strtotime($end);*/
            try {
                $sql = "SELECT id,title,description,className,start, end,allDay,Iduser
                        FROM calendario
                        WHERE start between :del AND :al
                        ORDER BY start, description";
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':del', $start, PDO::PARAM_STR);
                $stmt->bindParam(':al', $end, PDO::PARAM_STR);
                $rr=$stmt->execute();
                $datos=$stmt->fetchAll(PDO::FETCH_ASSOC);
                return $datos;
                //$db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                //return $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);

            }catch (Exception $e){
                return $e->getMessage();
            }
        }

		public function modificar($datos){
            $c= new conectar();
            $db=$c->conexionPDO();

            $Id=$datos['id'];
            //var_dump($Id);
            $iduser = $_SESSION['iduser'];
            $sql="UPDATE calendario SET title=:title, description=:description, className=:className, 
                    start=:start, end=:end, allDay=:allDay, Iduser=:iduser
                  WHERE id=:id";

            try {
                $db->beginTransaction(); // also helps speed up your inserts.
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':title', $datos['title'], PDO::PARAM_STR );
                $stmt->bindParam(':description', $datos['description'], PDO::PARAM_STR );
                $stmt->bindParam(':className', $datos['className'], PDO::PARAM_STR );
                $stmt->bindParam(':start', $datos['start'], PDO::PARAM_STR );
                $stmt->bindParam(':end', $datos['end'], PDO::PARAM_STR );
                $stmt->bindParam(':allDay', $datos['allDay'], PDO::PARAM_INT );
                $stmt->bindParam(':iduser', $iduser, PDO::PARAM_INT );

                $stmt->bindParam(':id', $Id);

                $res = $stmt->execute();
                var_dump($res);
//                $st = $db->query('SHOW WARNINGS');
                if($res) {
                    $db->commit();
                    return $Id;
                }
                else {
                    return -1;
                }
            }
            catch(PDOException $e)
            {
                //var_dump($e);
                return "Error: " . $e->getMessage();
            }
		}

        public function eliminar($Id){
            $c= new conectar();
            $db=$c->conexionPDO();

            $sql="DELETE from calendario where id=:id";
            try {
                $db->beginTransaction(); // also helps speed up your inserts.
                $stmt = $db->prepare($sql);

                $stmt->bindParam(':id', $Id);
                $res = $stmt->execute();
                if($res) {
                    $db->commit();
                    return $Id;
                }
                else
                    return -1;
            }
            catch(PDOException $e)
            {
                //var_dump($e);
                return "Error: " . $e->getMessage();
            }
		}


	}

 ?>