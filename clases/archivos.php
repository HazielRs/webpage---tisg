<?php


class archivos
{
    public function __construct(){
        setlocale(LC_ALL, 'es_PE', 'es');
        date_default_timezone_set('America/Lima');
    }

    public function upload_archive($datos, $url){
        $c= new conectar();
        $db=$c->conexionPDO();
        $fecha=date('Y-m-d h:i:s');
        $iduser = $_SESSION['iduser'];
        $insertID=-1;
        $sql="";
        var_dump($datos);
        try {
        $sql="INSERT INTO archivos (id_usuario,ruta,tipo,size,fecha_creacion) 
                 VALUES (:id_usuario, :ruta, :tipo, :size, :fecha_creacion);";
        $db->beginTransaction(); // also helps speed up your inserts.
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id_usuario', $iduser, PDO::PARAM_INT);
        $stmt->bindParam(':ruta', $url, PDO::PARAM_STR);
        $stmt->bindParam(':tipo', $datos['tipo'], PDO::PARAM_STR);
        $stmt->bindParam(':size', $datos['size'], PDO::PARAM_INT);
        $stmt->bindParam(':fecha_creacion',$fecha, PDO::PARAM_STR);
        $res = $stmt->execute();
        var_dump($res);
        //$stmt = $db->query('SHOW WARNINGS');
        //var_dump($res);
            if($res) {
                $insertID=$db->lastInsertId();
                $db->commit();
            }
            return $insertID;
        }
        catch(PDOException $e)
        {
            //var_dump($e);
        return "Error: " . $e->getMessage();
        }
    } //cambiar solo imagen

    public function ExisteTituloArchivo($titulo){
        $nrows=0;
        $c= new conectar();
        $db=$c->conexionPDO();

        $sql="SELECT count(*) FROM archivos and titulo=:titulo";

        try {
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":titulo", $titulo,PDO::PARAM_STR) ;
            $stmt->execute(); //true o false
            //$nrows=$stmt->rowCount();
            $nrows=$stmt->fetchColumn();
        }
        catch(PDOException $e)
        {
            //var_dump($e);
            $nrows=-1;
            //return "Error: " . $e->getMessage();
        }

        //$registro = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
        return $nrows;
    }

    public function obtenDatosArchivo($idarchivo){

        $c=new conectar();
        $db=$c->conexionPDO();
        try {
            $sql = "SELECT id, titulo, descripcion, tipo, size, fecha_creacion, ruta
					from archivos 
					where id='$idarchivo'";
            return $db->query($sql)->fetch(PDO::FETCH_ASSOC);

        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    public function TablaDatosArchivo(){

        $c=new conectar();
        $db=$c->conexionPDO();
        try {
            $sql = "SELECT a.id, a.titulo, a.descripcion, CONCAT(u.nombre, ' ', u.apellido) as author, a.tipo, a.size, a.fecha_creacion
					    from archivos as a 
					    LEFT JOIN usuarios as u on a.id_usuario = u.id_usuario
					    order by fecha_creacion DESC";
            return $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    public function eliminaArchivo($idarchivo){
        $c= new conectar();
        $db=$c->conexionPDO();

        $sql="DELETE from archivos where id=:id";
        try {
            $db->beginTransaction(); // also helps speed up your inserts.
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':id', $idarchivo);
            $res = $stmt->execute();
            if($res) {
                $db->commit();
                return $idarchivo;
            }
            else
                return -1;
        }
        catch(PDOException $e)
        {
            //var_dump($e);
            return "Error: " . $e->getMessage();
        }
    }

    public function actualizaArchivo($datos){
        //var_dump($datos);
        $c= new conectar();
        $db=$c->conexionPDO();
        $Id=$datos['id'];


        //var_dump($iduserCrea);
        $sql="UPDATE archivos SET titulo=?, descripcion=? WHERE id=?";
        try {
            $db->beginTransaction(); // also helps speed up your inserts.
            $stmt = $db->prepare($sql);
            $res = $stmt->execute(array($datos['titulo'],
                    $datos['descripcion'],
                    $Id)
            );
            if($res) {
                $db->commit();
                return $Id;
            }
            else
                return -1;
        }
        catch(PDOException $e)
        {
            //var_dump($e);
            return "Error: " . $e->getMessage();
        }
    }
}