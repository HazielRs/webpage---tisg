<?php 

	class usuarios{

        public function __construct(){
        setlocale(LC_ALL, 'es_PE', 'es');
        date_default_timezone_set('America/Lima');
        }

		public function registroUsuario($datos){
            $c= new conectar();
            $db=$c->conexionPDO();
			$fecha=date('Y-m-d h:i');

			$sql="INSERT into usuarios (username, nombre, apellido, email, password, fechaCaptura, activo, isadministrador, direccion, ciudad, telefono)
                VALUES (:username, :nombre, :apellido, :email, :password, :fechaCaptura, :activo, :isadministrador, :direccion, :ciudad, :telefono)";
            $datosInsertar=array(
                ":username" =>$datos['username'],
                ":nombre" =>$datos['nombre'],
                ":apellido" =>$datos['apellido'],
                ":email" => $datos['email'],
                ":password" => sha1($datos['password']),
                ":fechaCaptura" => $fecha,
                ":activo" => '1',
                ":isadministrador" => $datos['isadministrador'],
                ":direccion" => $datos['direccion'],
                ":ciudad" => $datos['ciudad'],
                ":telefono" => $datos['telefono'],
            );
            try {
                $stmt = $db->prepare($sql);
                $res = $stmt->execute($datosInsertar); //true o false
                if($res)
                    return $db->lastInsertId();
                else
                    return -1;
            }
            catch(PDOException $e)
            {
                //var_dump($e);
                return "Error: " . $e->getMessage();
            }
		}

		public function loginUser($datos){
			$c=new conectar();
			$db=$c->conexionPDO();

			$password=sha1($datos[1]);

            $sql="SELECT id_usuario, isadministrador from usuarios
				where email=:email and password=:password and activo=1";
            //Para evitar inyección de dependencias (evitar hackeos)
            $stmt = $db->prepare($sql);
            $stmt->bindParam("email", $datos[0],PDO::PARAM_STR);
            $stmt->bindParam("password", $password,PDO::PARAM_STR);
            $stmt->execute();

            $count=$stmt->rowCount();
            $res=$stmt->fetch(PDO::FETCH_ASSOC);

			//$result=mysqli_query($conexion,$sql);
			//if(mysqli_num_rows($result) > 0){
            if($count>0){
                //almacenar datos de cookies
                $_SESSION['usuario']=$datos[0];
                $_SESSION['iduser']=$res['id_usuario'];
                $_SESSION['isadministrador']=$res['isadministrador'];

			    return 1;
			}else{
				return 0;
			}
		}

        public function traeID($datos){
			$c=new conectar();
			$db=$c->conexionPDO();

			$password=sha1($datos[1]);

			$sql="SELECT id_usuario 
					from usuarios 
					where email=:email
					and password=:password
					and activo='1'";

            try {
                $stmt = $db->prepare($sql);
                $stmt->bindParam("email", $datos[0],PDO::PARAM_STR) ;
                $stmt->bindParam("password", $password,PDO::PARAM_STR) ;
                $stmt->execute(); //true o false
                $count=$stmt->rowCount();
                $res=$stmt->fetch(PDO::FETCH_ASSOC);
                return $res;
                }
            catch(PDOException $e)
            {
                //var_dump($e);
                return "Error: " . $e->getMessage();
            }
			//$result=mysqli_query($conexion,$sql);
			//return mysqli_fetch_row($result);
		}

        public function obtenDatosUsuario($idusuario){

            $c=new conectar();
            $db=$c->conexionPDO();
            try {
                $sql = "SELECT id_usuario, username, nombre, apellido, email, fechaCaptura, 
                          activo, isadministrador, password, direccion, ciudad, telefono, about
					from usuarios 
					where id_usuario='$idusuario'
					ORDER BY username DESC";
                return $db->query($sql)->fetch(PDO::FETCH_ASSOC);

            }catch (Exception $e){
                return $e->getMessage();
            }
        }

        public function TablaDatosUsuario(){

            $c=new conectar();
            $db=$c->conexionPDO();
            try {
                $sql = "SELECT id_usuario, username, nombre, apellido, email, activo, isadministrador, telefono
					    from usuarios order by  nombre, apellido";
                return $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);

            }catch (Exception $e){
                return $e->getMessage();
            }
        }

        public function obtenDatosUsuarioPorEmail($email){
            $c= new conectar();
            $db=$c->conexionPDO();

            $sql="SELECT id_usuario, nombre, apellido,email, fechaCaptura, activo, isadministrador
                    FROM usuarios
					where email=:$email";
            $registro = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
            return $registro;
        }

        public function ExisteEmail($email){
            $nrows =0;
            $c= new conectar();
            $db=$c->conexionPDO();

            $sql="SELECT count(*) FROM usuarios where activo=1 and email=:email";
            
            try {
                $stmt = $db->prepare($sql);
                $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
                $stmt->execute(); //true o false
                //$nrows=$stmt->rowCount();
                $nrows=$stmt->fetchColumn();
                }
            catch(PDOException $e)
            {
                //var_dump($e);
                $nrows=-1;
                //return "Error: " . $e->getMessage();
            }

            //$registro = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
            return $nrows;
        }

        public function ExisteUserName($username){
            $nrows =0;
            $c= new conectar();
            $db=$c->conexionPDO();

            $sql="SELECT count(*) FROM usuarios
                    where activo=1 and username=:username";
            
            try {
                $stmt = $db->prepare($sql);
                $stmt->bindParam("username", $username,PDO::PARAM_STR) ;
                $stmt->execute(); //true o false
                //$nrows=$stmt->rowCount();
                $nrows=$stmt->fetchColumn();
                }
            catch(PDOException $e)
            {
                //var_dump($e);
                $nrows=-1;
                //return "Error: " . $e->getMessage();
            }

            //$registro = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
            return $nrows;
        }

		public function actualizaUsuario($datos){
            //var_dump($datos);
            $c= new conectar();
            $db=$c->conexionPDO();
            $fecha=date('Y-m-d h:i:s');
            $Id=$datos['id_usuario'];

            $iduserCrea = $_SESSION['iduser'];
            //var_dump($iduserCrea);
            $sql="UPDATE usuarios SET nombre=?, apellido=?, isadministrador=?
                  ,password=? ,direccion=? ,ciudad=?, telefono=?, about=?, activo=? WHERE id_usuario=?";
            try {
                $db->beginTransaction(); // also helps speed up your inserts.
                $stmt = $db->prepare($sql);
                $res = $stmt->execute(array($datos['nombre'],
                                        $datos['apellido'],
                                        $datos['isadministrador'],
                                        $datos['password'],
                                        $datos['direccion'],
                                        $datos['ciudad'],
                                        $datos['telefono'],
                                        $datos['about'],
                                        $datos['activo'],
//                                        $iduserCrea,
                                        $Id)
                                    );
                if($res) {
                    $db->commit();
                    return $Id;
                }
                else
                    return -1;
            }
            catch(PDOException $e)
            {
                //var_dump($e);
                return "Error: " . $e->getMessage();
            }
		}

		public function eliminaUsuario($idusuario){
            $c= new conectar();
            $db=$c->conexionPDO();

			$sql="DELETE from usuarios where id_usuario=:id";
            try {
                $db->beginTransaction(); // also helps speed up your inserts.
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':id', $idusuario);
                $res = $stmt->execute();
                if($res) {
                    $db->commit();
                    return $idusuario;
                }
                else
                    return -1;
            }
            catch(PDOException $e)
            {
                //var_dump($e);
                return "Error: " . $e->getMessage();
            }
		}
	}

 ?>