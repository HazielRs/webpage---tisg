<?php
session_start();
require_once (dirname(__FILE__) . "/clases/conexion.php");
require_once (dirname(__FILE__) . "/clases/articulos.php");
require_once (dirname(__FILE__) . "/clases/paginar_articulos.php");
$c = new conectar();
$articulo = new articulos();
$pagina = (isset($_GET['page'])) ? $_GET['page'] : 1;
$enlaces = (isset($_GET['enlaces']))? $_GET['enlaces'] : 5;
$tagactivo = (isset($_GET['tag'])) ? $_GET['tag'] : '';
$paginar = new PaginarArticulos($articulo, $enlaces);
$resultados = $paginar->getDatos($pagina);
//var_dump($resultados);
?>
<!doctype html>
<html lang="es_PE">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="img/favicon.png" type="image/png">
    <title>Ministerio Doulos - Id y hacer Discipulos a todas las naciones</title>
    <base href="<?php echo $c->urlbase;?>">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/my.css">
    <link rel="stylesheet" href="css/redessociales.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/835ac9bcc2f5ac466178e6578/c48fd358b3ec665fb141d17c7.js");</script>
</head>
<body>
<?php include ("snippets/navbar.php"); ?>

<div id="blog">
    <div id="webcontent" class="webcontent">
        <!--================ CONTENIDO A PARTIR DE AQUI =================-->
        <!--================Small Header Area =================-->
        <section class="banner_area">
            <div class="box_1620">
                <div class="banner_inner d-flex align-items-center">
                    <div class="container">
                        <div class="banner_content text-center">
                            <h2>Nuestro Blog</h2>
                            <div class="page_link">
                                <a href="index.php">Inicio</a>
                                <a href="blog.php">Blog</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================Small Header Area =================-->
        <!--================Blog Area =================-->
        <section class="blog_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="blog_left_sidebar mt-4">
                            <aside class="single_sidebar_widget search_widget">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Busca tus posts favoritos por palabras clave">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="lnr lnr-magnifier"></i></button>
                                    </span>
                                </div><!-- /input-group -->
                                <div class="br"></div>
                            </aside>
                            <br />
                            <?php
                            if($resultados->datos){
                                foreach ($resultados->datos as $row)
                                {
                                    //var_dump($row);
                                    ?>
                                    <article class="row blog_item">
                                        <div class="col-md-3">
                                            <div class="blog_info text-right">
                                                <?php
                                                $tags = explode(",",$row['Tags_Art']);
                                                if($tags){
                                                    echo "<div class='post_tag'>";
                                                    foreach ($tags as $indTag=>$tag)
                                                    {
                                                    ?>
                                                    <a href="#"
                                                    <?php echo $tagactivo!='' && $tagactivo==$tag ? "class='active'" : ''; ?>
                                                    ><?php
                                                        echo $indTag!=0 ? ", " : "";
                                                        echo trim($tag);
                                                    ?></a>
                                                    <?php
                                                    } //Fin de foreach
                                                    echo "</div>";
                                                    } //Fin del If
                                                    ?>
                                                <ul class="blog_meta list">
                                                    <li><a href="#"><?php echo $row['author']?><i class="lnr lnr-user"></i></a></li>
                                                    <li><a href="#"> <span id="fechapublicacion"><?php echo $row['FechaPublicacion_Art']?></span> <i class="lnr lnr-calendar-full"></i></a></li>
                                                    <li><a href="#"><?php echo $row['Visitas_Art']?><i class="lnr lnr-eye"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="blog_post">
                                                <img style="max-height: 350px; max-width: 750px; width: 90%; height: auto;"
                                                     class="img-fluid img-thumbnail" src="<?php echo $row['UrlImg_Art']?>" alt="...">
                                                <div class="blog_details">
                                                    <a href="blog-details.php"><h2><?php echo $row['Titulo_Art']?></h2></a>
                                                    <?php echo $row['Introtext_Art']?>
                                                    <a href="blog-details.php" class="white_bg_btn">Ver más</a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                <?php }
                            }
                            ?>
                            <nav class="blog-pagination justify-content-center d-flex">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a href="#" class="page-link" aria-label="Previous">
		                                    <span aria-hidden="true">
		                                        <span class="lnr lnr-chevron-left"></span>
		                                    </span>
                                        </a>
                                    </li>
                                    <li class="page-item"><a href="#" class="page-link">01</a></li>
                                    <li class="page-item active"><a href="#" class="page-link">02</a></li>
                                    <li class="page-item"><a href="#" class="page-link">03</a></li>
                                    <li class="page-item"><a href="#" class="page-link">04</a></li>
                                    <li class="page-item"><a href="#" class="page-link">09</a></li>
                                    <li class="page-item">
                                        <a href="#" class="page-link" aria-label="Next">
		                                    <span aria-hidden="true">
		                                        <span class="lnr lnr-chevron-right"></span>
		                                    </span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!--================Blog Area =================-->
        <!--================Instagram Area =================-->
        <?php require_once "snippets/InstaArea.php"; ?>
        <!--================End Instagram Area =================-->
        <!--================ FIN DE CONTENIDO A PARTIR DE AQUI =================-->
    </div>
</div>
<br/>
<?php require_once "snippets/footer.php"; ?>
<!-- Optional JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="vendors/lightbox/simpleLightbox.min.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="dashboard/assets/vendor/moment/min/moment.min.js"></script>
<script src="dashboard/assets/vendor/moment/locale/es.js"></script>
<script src="js/theme.js"></script>
<!--My JS-->
<script src="js/proyectoweb.js"></script>
<script>
    /*Script de Autoejecucion cuando cargue la pagina*/
    $(function () {
        //marcar menu con clase active, dara color ROJO
        $("#nav_blog").addClass("active");

        var fecha = document.getElementById("fechapublicacion").innerHTML;
        //var fecha = $('#fechapublicacion').innerText;
        var publicacion = moment(fecha).format("LLLL");
        document.getElementById("fechapublicacion").innerHTML.replace();

</script>
</body>
</html>