<?php
session_start();
require_once (dirname(__FILE__) . '/clases/conexion.php');
$c = new conectar();?>
<!doctype html>
<html lang="es_PE">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="img/favicon.png" type="image/png">
    <title>Ministerio Doulos - Id y hacer Discipulos a todas las naciones</title>
    <base href="<?php echo $c->urlbase;?>">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/animate-css/animate.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/my.css">
    <link rel="stylesheet" href="css/redessociales.css">
    <link rel="stylesheet" href="css/archives.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/835ac9bcc2f5ac466178e6578/c48fd358b3ec665fb141d17c7.js");</script>
</head>
<body>
<?php include ("snippets/navbar.php"); ?>
    <div class="contenedor">
        <div class="card-container">
                <div class="card"><a href="hottub">
                        <div class="card--display"><i class="material-icons">hot_tub</i>
                            <h2>Hot Tub Time Machine</h2>
                        </div>
                        <div class="card--hover">
                            <h2>Hot Tub Time Machine</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam at est orci. Nam molestie pellentesque mi nec lacinia. Cras volutpat arcu sit amet elit sodales, nec volutpat velit bibendum.</p>
                            <p class="link">Click to see project</p>
                        </div></a>
                    <div class="card--border"></div>
                </div>
            </div>
        <div class="card-container">
                <div class="card"><a href="worldweb">
                        <div class="card--display"><i class="material-icons">public</i>
                            <h2>World Wide Web</h2>
                        </div>
                        <div class="card--hover">
                            <h2>World Wide Web</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam at est orci. Nam molestie pellentesque mi nec lacinia. Cras volutpat arcu sit amet elit sodales, nec volutpat velit bibendum.</p>
                            <p class="link">Click to see project</p>
                        </div></a>
                    <div class="card--border"></div>
                </div>
            </div>
        <div class="card-container">
                <div class="card"><a href="trainthings">
                        <div class="card--display"><i class="material-icons">train</i>
                            <h2>Why I Hate Trains</h2>
                        </div>
                        <div class="card--hover">
                            <h2>Why I Hate Trains</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam at est orci. Nam molestie pellentesque mi nec lacinia. Cras volutpat arcu sit amet elit sodales, nec volutpat velit bibendum.</p>
                            <p class="link">Click to see project</p>
                        </div></a>
                    <div class="card--border"></div>
                </div>
            </div>
        <div class="card-container">
                <div class="card card--dark"><a href="androidupdate">
                        <div class="card--display"><i class="material-icons">android</i>
                            <h2>Another Android Update</h2>
                        </div>
                        <div class="card--hover">
                            <h2>Another Android Update</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam at est orci. Nam molestie pellentesque mi nec lacinia. Cras volutpat arcu sit amet elit sodales, nec volutpat velit bibendum.</p>
                            <p class="link">Click to see project</p>
                        </div></a>
                    <div class="card--border"></div>
                </div>
            </div>
    </div>
<br/>
<?php include ("snippets/footer.php"); ?>
    <!-- Optional JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/theme.js"></script>
    <!--My JS-->
    <script src="js/proyectoweb.js"></script>
    <script>
        /*Script de Autoejecucion cuando cargue la pagina*/
        $(function () {
            //marcar menu con clase active, dara color ROJO
            $("#nav_archivos").addClass("active");
        });
    </script>
</body>
</html>
