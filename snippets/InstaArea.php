<section class="instagram_area">
    <div class="container box_1620">
        <div class="insta_btn">
            <a class="btn theme_btn" href="#">Síguenos en nuestro instagram</a>
        </div>
        <div class="instagram_image row justify-content-center">

            <a href="#"><img src="img/instagram/ins-2.jpg" alt=""></a>
            <a href="#"><img src="img/instagram/ins-3.jpg" alt=""></a>
            <a href="#"><img src="img/instagram/ins-4.jpg" alt=""></a>
            <a href="#"><img src="img/instagram/ins-5.jpg" alt=""></a>

        </div>
    </div>
</section>
