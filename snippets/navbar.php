<?php require_once "socialbar.php"; ?>
<input type="hidden" id="_Alerta" value="" />
<div class="up">
    <span class="fa fa-chevron-up">
    </span>
</div>
<!--================Header Menu Area =================-->
<header id="header" class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- observacion
                    data-target="#navbarSupportedContent"
                    es el nombre que se expandera en moviles
                -->
                <!-- NAV IZQUIERDA ---Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto mr-auto">
                        <li class="nav-item submenu dropdown" id="nav_about">
                            <a href="#" class="nav-link dropdown-toggle"
                               data-toggle="dropdown" role="button" aria-haspopup="true"
                               aria-expanded="false">Nosotros</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a data-scroll class="nav-link" href="../index.php/#about-us">Quiénes Somos</a>
                                </li>
                                <li class="nav-item">
                                    <a data-scroll class="nav-link" href="../index.php/#our-projects">Algunos Proyectos</a>
                                </li>
                            </ul>
                        </li>
                        <li id="nav_testimonies" class="nav-item">
                            <a data-scroll class="nav-link" href="../index.php/#testimonies">Testimonios</a>
                        </li>
                        <li id="nav_contact" class="nav-item">
                            <a class="nav-link" href="../contact.php">Contactenos</a>
                        </li>
                        <li id="nav_projects" class="nav-item">
                            <a class="nav-link" href="../projects.php">Proyectos</a>
                        </li>
                    </ul>
                </div>

                <!-- nav CENTER LOGO -Brand and toggle get grouped for better mobile display -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContentCenter">
                    <div class="align-content-center" style="margin: 0 auto;">
                        <a class="navbar-brand logo_h" href="../index.php">
                            <img class="logoDM" src="../img/LogoW.png" alt="Doulos">
                        </a>
                    </div>
                </div>

                <!-- NAV DERECHA ---Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto mr-auto">
                        <li id="nav_eventos" class="nav-item"><a class="nav-link" href="../events.php">Eventos</a></li>
                        <li id="nav_blog" class="nav-item"><a class="nav-link" href="../blog.php">Blog</a></li>
                        <?php if(isset($_SESSION['usuario']))
                        echo '<li id="nav_archivos" class="nav-item"><a class="nav-link" href="../archives.php">Archivos</a></li>';?>
                        <?php if(isset($_SESSION['usuario'])){
                            echo '<li class="nav-item"><a class="nav-link" href="../logout.php">Logout</a></li>';
                        } else {
                            echo '<li id="nav_admin" class="nav-item"><a class="nav-link" href="../dashboard/procesos/ingresar.php">Regístrate</a></li>';
                            }
                        ?>
                        <?php if(isset($_SESSION['isadministrador']) && $_SESSION['isadministrador']=="1"){
                            echo '<li class="nav-item"><a class="nav-link" href="../dashboard/dashboard.php">Dashboard</a></li>';
                        }?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!--================Header Menu Area =================-->
