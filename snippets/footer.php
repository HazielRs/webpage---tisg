<!--================Footer Area =================-->
<footer class="footer_area" id="footer">
    <div class="container">
        <div class="row footer_inner">
            <div class="col-lg-6 col-sm-6">
                <aside class="f_widget ab_widget">
                    <div class="f_title">
                        <h3>Acerca de Nosotros</h3>
                    </div>
                    <p>Somos un grupo de adolescentes que buscan marcar el cambio en la sociedad, mediante actividades recreativas y de interacción
                        con nuestra comunidad.</p>
                </aside>
            </div>
            <div class="col-lg-6 col-sm-6">
                <aside class="f_widget news_widget">
                    <div class="f_title">
                        <h3>Newsletter</h3>
                    </div>
                    <p>¡Recibe updates mensuales de materiales específicos sobre nuestras actividades y materiales de liderazgo juvenil!</p>
                    <div id="mc_embed_signup">
                        <form target="_blank" action="https://www.us4.list-manage.com/subscribe/post?u=835ac9bcc2f5ac466178e6578&amp;id=bee61987f6" method="get" class="subscribe_form relative">
                            <div class="input-group d-flex flex-row">
                                <input name="EMAIL" placeholder="Ingrese su E-Mail" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Correo Electronico '" required="" type="email">
                                <button class="btn sub-btn"><span class="lnr lnr-arrow-right"></span></button>
                            </div>
                            <div class="mt-10 info"></div>
                        </form>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</footer>
<!--================End Footer Area =================-->
